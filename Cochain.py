class Cochain(dict):
    def __mul__(self,another):
        result = Cochain(self)
        for key in result:
            result[key]*=another
        return result
        
    def __neg__(self): #invoked on -self
        result = Cochain(self)
        result.invert()
        return result
    
    # enlarges a cochain (no output)
    def add_singleton(self,key,value):
        self[key] = self.get(key,0) + value
        
    # adds another cochain to cochain (no output)
    def add_cochain(self,cochain):
        keys = set(self.keys()+cochain.keys())
        for key in keys:
            self[key] = self.get(key,0)+cochain.get(key,0)
            
    # inverts values in a cochain (no output)
    def invert(self):
        for key in self:
            self[key] = -self[key]
            
    # returns the cochain mod prime 
    def mod_p(self,prime):
        if prime==0:
            return self
        out=Cochain({})
        for key in self:
            mod_prime=self[key] % prime
            if mod_prime != 0:
                out[key]=mod_prime
        return out

def cochain_conversion(cochain,dic):
    out=Cochain({})
    for item in cochain:
        out.add_singleton(dic[item],cochain[item])
    return out
    
def cochain_addition(c1, c2):
    out=Cochain({})
    keys = set(c1.keys()) | set(c2.keys())
    for key in keys:
        sum = c1.get(key,0)+c2.get(key,0)
        if sum!=0:
            out[key]=sum
    return out

def cochain_subtraction(c1, c2):
    out=Cochain({})
    keys = set(c1.keys()) | set(c2.keys())
    for key in keys:
        sum = c1.get(key,0)-c2.get(key,0)
        if sum!=0:
            out[key]=sum
    return out

def remove_zeros(cochain):
    out=Cochain({})
    for item in cochain:
        if cochain[item] != 0:
            out[item]=cochain[item]
    return out

if __name__=="__main__":
    print("ahoj")
    c1=Cochain({1:2})
    c2=Cochain({1:2, 3:4})
    #print(set(c1.keys()))
    print(cochain_addition(c1,c2))