from Cochain import *
from sequences import subsets_nonempty, permutation_sign
from reduction import AW_matrix
from simplex import *
from filtration import *
from cubical_sc import *
from cubical_sc_torus import *
import operator

def codifferential_cubic(sc,cochain):
    output=Cochain({})
    for item in cochain:
        cob_it=sc.cubical_coboundary(item)
        for cube in cob_it:
            if not cube[0] in output:
                cube_bound=sc.cubical_boundary(cube[0])
                count=0
                for subcube in cube_bound:
                    if subcube[0] in cochain:
                        count+=cochain[subcube[0]]*subcube[1]
                if count !=0:
                    output[cube[0]]=count
    return output

def codifferential_simplicial(sc,cochain,torus):
    def canonicize(sim):
        vert=sim.vertex_
        new_vert=[vert[j] if vert[j]<sc.shape[j] else 0 for j in range(sc.m)]
        return simplex(new_vert,sim.sequence_)
    output=Cochain({})
    for item in cochain:
        cob_it=sc.coboundary(item)
        for sim in cob_it:
            if not sim[0] in output:
                sim_bound=sim[0].boundary()
                count=0
                for subsim in sim_bound:
                    subs=subsim[0]
                    if torus:
                        subs=canonicize(subs)
                    if subs in cochain:
                        count+=cochain[subs]*subsim[1]
                if count !=0:
                    output[sim[0]]=count
    return output

def project_to_cubes(sc,cochain):
    # Eilenberg Zilberg: simpl. cochain --> cubical cochain
    out=Cochain({})
    for sim in cochain:
        sm_s=sc.smallest_supercube(sim)
        if sm_s not in out and sc.cube_dim(sm_s)==sim.dim:
            subdiv=sc.simplices_of_cube(sm_s)
            count=0
            for item in subdiv:
                if item[0] in cochain:
                    count+=cochain[item[0]]*item[1]
            if count !=0:
                out.add_singleton(sm_s,count)
    return out

def project_to_simplices(sc,cochain,degree,torus):
    # Alexander-Whitney: cubical cochain --> simpl. cochain
    out=Cochain({})
    if degree==0: # in dimension zero, we convert cube-vertices to simplex-vertices
        for item in cochain:
            sim=simplex(item.vertex(),[])
            out[sim]=cochain[item]
        return out
    # dim > 0
    AW=AW_matrix(sc,sc.m,degree)
    for cube in cochain:
        value=cochain[cube]
        simplices2sign=AW[tuple(sc.cube_span(cube))]
        for sim in simplices2sign:
            new_vertex=sim.vertex(0)+sc.cube_vertex(cube)
            if sc.vertex_in_sc(new_vertex) or torus:
                if torus:
                    new_vertex=sc.module(new_vertex) # canonify
                new_sim=simplex(new_vertex,sim.sequence_)
                val=value*simplices2sign[sim]
                if val != 0 and sc.simplex_in_sc(new_sim):
                    out.add_singleton(new_sim, val)
    return remove_zeros(out)

def SHI_linear_problem(sc,func_val_abs,fo,rhs,n):
    def order2items(dic): # transforms {0:3.2,12:1.5,15:1.6} to [12, 15, 0]
        out=[0]*len(dic)
        dic_sorted = sorted(dic.items(), key=operator.itemgetter(1)) 
        ids=0
        for item in dic_sorted:
            out[ids]=dic_sorted[ids][0]
            ids+=1
        return out

    # generating a directory {n-1 simplices near the support of fo : r}
    nm1_near_fo=dict()
    for sim1 in fo:
        sm_s=sc.smallest_supercube(sim1)
        Star=sc.cubical_star(sm_s)
        for cube in Star:
            simplices=sc.k_simplices_of_cube(cube,n-1)
            for sim in simplices:
                if not sim in nm1_near_fo:
                    nm1_near_fo[sim]=simplex_filtration(func_val_abs,sim)

    # coboundaries and matrix construction
    matrix=[[] for i in range(len(nm1_near_fo))] # initialization of the zero matrix
    nm1_ids2sim=order2items(nm1_near_fo) # list of simplices ordered by filtration    
    n_sim2ids=dict()
    ids=0
    for column in range(len(nm1_ids2sim)):
        sim_cob=sc.coboundary(nm1_ids2sim[column])
        for sim in sim_cob: # n-simplex near fo
            if not sim[0] in n_sim2ids:
                n_sim2ids[sim[0]]=ids
                ids+=1
            matrix[column].append((n_sim2ids[sim[0]],(-1)**sim[1]))
    vector=cochain_conversion(rhs,n_sim2ids)
    return matrix,vector,nm1_ids2sim


def check_cocycle(sc,cochain,func_val_abs,r,torus,prime=0):
    def transform_vertex(b):
        sim,val=b
        vert=sim.vertex_
        new_vert=np.array([vert[i] if vert[i]<sc.shape[i] else 0 for i in range(sc.m)])
        new_sim=simplex(new_vert,sim.sequence_)
        return new_sim,val
        
    k_plus_one=set() # this will be the *set* of k+1 simplices for testing cocyclicity
    for c in cochain:
        cobound=sc.coboundary(c)
        for cob in cobound:
            k_plus_one.add(cob[0])
#    k_plus_one=sc.k_simplices_list(k+1)
    for sim in k_plus_one:
        if simplex_in_A(func_val_abs,sim,r):
            bound=sim.boundary() # list of pairs (simplex,sign)
            summ=0
            for b in bound:
                if torus:
                    b=transform_vertex(b)
                if b[0] in cochain:
                    summ=summ+cochain[b[0]]*b[1]
            if prime>0:
                summ = summ % prime
            if summ != 0:
                print("non-cocyclicity detected")
                print("simplex:", sim.print_sim())
                s_h=sc.smallest_supercube(sim)
                print("smallest supercube:", sc.cube_twice_barycenter(s_h))
                print("function values:")
                for v in sc.cube_vertices(s_h):
                    print(v, func_val_abs[tuple(v)])
                for b in bound:
                    print(b[0].print_sim())
                    if b[0] in cochain:
                        print("cochain[b[0]]=", cochain[b[0]])
                    else:
                        print("not in the cochain..")

                return False
    return True

def check_cocycle_cubic(sc,cochain,func_val_abs,r,prime=0):
    k_plus_one=set() # this will be the *set* of k+1 cubes for testing cocyclicity
    for c in cochain: 
        cobound=sc.cubical_coboundary(c)
        for cob in cobound:
            k_plus_one.add(cob[0])
#    k_plus_one=sc.k_simplices_list(k+1)
    for cub in k_plus_one:
        if cube_in_A(func_val_abs,sc,cub,r):
            bound=sc.cubical_boundary(cub) # list of pairs (cube,sign)
            summ=0
            for b in bound:
                if b[0] in cochain:
                    summ=summ+cochain[b[0]]*b[1]
            if prime>0:
                summ=summ % prime
            if summ != 0:
                print("non-cocyclicity detected")
                print("cube:", cub.barycenter())
                print("function values:")
                for v in cub.vertices():
                    print(v, func_val_abs[tuple(v)])
                for b in bound:
                    print(b[0].barycenter())
                    if b[0] in cochain:
                        print("cochain[b[0]]=", cochain[b[0]])
                    else:
                        print("not in the cochain..")
                return False
    return True

def cochain_zero_in_A(func_val_abs,cochain,r):
    # testing whether a k-cocycle maps all k-simplices in A to zero
    for item in cochain:
        if simplex_in_A(func_val_abs,item,r) and cochain[item] !=0:
            return False
    return True

def cochain_zero_in_A_cubic(sc,func_val_abs,cochain,r):
    # testing whether a k-cochain maps all k-simplices in A to zero
    for item in cochain:
        if cube_in_A(func_val_abs,sc,item,r) and cochain[item] !=0:
            return False
    return True

def min_simplic_r(sc,func_val_abs,func_sim):
    out=-1.0
    subs=subsets_nonempty([0]*sc.m,Inf)
    vertices=sc.vertices()
    for vert in vertices:
        if func_val_abs[tuple(vert)]>out:
            for s in subs:
                new_vert=vert+np.array(s)
                if func_val_abs[tuple(new_vert)]>=out:
                    if func_sim[tuple(vert)]==-func_sim[tuple(new_vert)]:
                        sim=simplex(np.array(vert),[s])
                        if simplex_in_A(func_val_abs,sim,out):
                            out=max(out,simplex_filtration(func_val_abs,sim))
    return out
 
def check_simplicial(sc,func_val_abs,func_sim,r):
    subs=subsets_nonempty([0]*sc.m,Inf)
    vertices=sc.vertices()
    for vert in vertices:
        if func_val_abs[tuple(vert)]>=r:
            for s in subs:
                new_vert=vert+np.array(s)
                if func_val_abs[tuple(new_vert)]>=r:
                    if func_sim[tuple(vert)]==-func_sim[tuple(new_vert)]:
                        sim=simplex(np.array(vert),[s])
                        if simplex_in_A(func_val_abs,sim,r):
                            print("|f|(", vert, ")=", func_val_abs[tuple(vert)])
                            print("f'(v)=", func_sim[tuple(vert)])
                            print("|f|(", new_vert, ")=", func_val_abs[tuple(new_vert)])
                            print("f'(w)=", func_sim[tuple(new_vert)])
                            return False
    return True

def pullback(sc, func_val_abs, func_sim, n, r):
# returns the pullback of the fundamental top cohomology class by the function f': A --> S^{n-1}
    first_moves=subsets_nonempty([0]*sc.m,sc.m-n+2)
    graph=graph_of_sequences(sc.m,n-1)
    support=set()

    def pull_rec(sim,subset,depth):# no direct output, just increases the "support" Cochain
        if depth==n-1:
            if simplex_in_A(func_val_abs,sim,r):
                support.add(sim)
            return
        if depth==0:
            moves=first_moves
        else:
            moves=graph[depth-1][tuple(sim.sequence_[-1])] #
        for move in moves:
            new_vert=sim.vertex(0)+np.array(move)
            if func_val_abs[tuple(new_vert)] >= r:
                f_new_sim=func_sim[tuple(new_vert)]
                if f_new_sim > 0 and (not f_new_sim in subset):
                    new_seq=copy(sim.sequence_)
                    new_seq.append(move)
                    new_subs=copy(subset)
                    new_subs.add(f_new_sim)
                    pull_rec(simplex(sim.vertex(0), new_seq), new_subs, depth+1)

    for vert in sc.vertices():
        if vertex_in_A(func_val_abs,vert,r):
            sim_val=func_sim[tuple(vert)]
            if sim_val>0:
                pull_rec(simplex(np.array(vert), []), set([sim_val]),0)
    output=Cochain({})
    for sim in support:
        values=[func_sim[tuple(sim.vertex(i))] for i in range(n)]
        output.add_singleton(sim,permutation_sign(values))
    return output

def matrix_times_vector(matrix,vector):
# matrix is a list of columns, a column is a list of pairs (row-index, value)
# vector is a dictionary
   def add_kx_to_vect(k,x,vect): # k is an integer, x and vect are vectors
      for item in x:
         if item in vect: 
            new_val=vect[item]+k*x[item]
            if new_val!=0: vect[item]=new_val
            else: del vect[item]
         else: 
            new_val=k*x[item]
            if new_val!=0: vect[item]=new_val
   result={}
   for item in vector:
      add_kx_to_vect(vector[item],dict(matrix[item]),result)
   return result


def matrix_times_matrix(matrix1,matrix2):
    res=list()
    for column in matrix2:
        res.append(matrix_times_vector(matrix1,dict(column)))
    return res

def isprime(n):
    if n < 2:
        return False
    elif n == 2: 
        return True    
    elif not n & 1: 
        return False
    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True
    
if __name__=="__main__":
    matrix1=np.load("matrix1.npy")
    matrix2=np.load("matrix2.npy")
    list1=np.load("list1.npy")
    list2=np.load("list2.npy")
    print(len(matrix1)) 
    print(len(matrix2)) 
    print(len(list1)) 
    print(len(list2)) 
    t=time() 
    #prod=matrix_times_matrix(matrix1,matrix2)
    #print(time()-t)
    #for item in prod:
    #   assert item=={}
    #rr=open('result', 'w')
    #rr.write('ok')
    #rr.close()
