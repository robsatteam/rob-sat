# Similar file then cubical_sc.py, but all methods are adjusted for a torus

import numpy as np
from time import time,sleep
from simplex import *
from sequences import *
from itertools import *

class cubical_sc_torus:
    def __init__(self,shape):
        self.shape = shape
        self.m = len(shape)
        self.offsets=[]
        for i in range(self.m+1):
            if i<self.m:
                assert self.shape[i]>1, 'shape is too small'
            if i==0:
                self.offsets.append(1)
            else:
                self.offsets.append((2*self.shape[i-1])*self.offsets[i-1])
            

    def vertex_in_sc(self, vertex): # Decides whether the vertex is in the simplicial complex
        assert(len(vertex)==self.m), 'dimension of the vertex and simp. complex disagree'
        for i in range(self.m):
            if vertex[i]<0 or vertex[i]>self.shape[i]:
                return False
        return True
     
    def simplex_in_sc(self,simplex):
        return self.vertex_in_sc(simplex.vertex_first()) and self.vertex_in_sc(simplex.vertex_last())
        
    def cube_in_sc(self,c):
        return c>=0 and c < np.prod([2*self.shape[i] for i in range(self.m)])
                
    def vertices(self):
        axes=[range(self.shape[i]) for i in range(self.m)]
        return list(product(*axes))
            
    def coboundary(self,sim): # ADJUSTED FOR TORUS
    # for a simplex, it returns a list of (sim, k) where
    # simplex is the k-th boundary of sim
        assert(self.simplex_in_sc(sim)), 'simplex not in simplicial complex'
        def minus_one_to_zero(vect):
            # [0,-1,0,1] --> [0,0,0,1]
            return [0 if x==-1 else x for x in vect] 

        def positions_of_newly_added_ones(sequence, i):
             # outputs a vector of zeros on positions where sequence[i] has a one and sequence[i-1] has not,
             # and -1's elsewhere
            if i==0:
                return [0 if x==1 else -1 for x in sequence[i]] # change ones and zeros
            else: # i>0
                return [0 if x==1 else -1 for x in (np.array(sequence[i])-np.array(sequence[i-1]))]
        # prints positions_of_newly_added_ones([[0,1,0,0,1,0], [1,1,0,0,0,0], [1,1,1,0,1,1]],2)

        if sim.dim==len(self.shape):
            return [] # no coboundaries of top dimensional simplices

        output=list()
        # We enrich the sequence with an "intermediate elements" -- here we don't need to care about forbidden directions
        for i in range(sim.dim):
            positions=positions_of_newly_added_ones(sim.sequence_,i) # vectors of -1 and 0's
            intermediate=subsets_nonempty(positions,positions.count(0)-1)
            for inter_vect in intermediate:
                inter_vect=minus_one_to_zero(inter_vect) # kill the minus ones
                sequence_new=list()
                for j in range(sim.dim+1):
                    if j<i:
                        sequence_new.append(sim.sequence_[j])
                    elif j==i and j==0:
                        sequence_new.append(inter_vect)
                    elif j==i and j>0:
                        sequence_new.append((list(np.array(sim.sequence_[i-1])+np.array(inter_vect))))
                    else: # j>i:
                        sequence_new.append(sim.sequence_[j-1])
                output.append((simplex(sim.vertex_, sequence_new), i+1))
        
        # Next we create enriched sequences by adding something to the last position of the sequence
        if sim.dim==0:
            temp_vect=[0]*self.m
        else:
            temp_vect=sim.sequence_[-1]
        intermediate_last=subsets_nonempty(temp_vect,np.Inf)
        # generates all nonempty subsets of zeros of last element of the sequence
        for inter_last_vect in intermediate_last:
            #sequence_new=deepcopy(sim.sequence_)
            sequence_new=copy(sim.sequence_)
            sequence_new.append(inter_last_vect)
            output.append((simplex(sim.vertex_, sequence_new), sim.dim+1))

        # Finally, we need to find all coboundaries where the vertex is changed
        if sim.dim==0:
            subtr_dir=[0]*self.m
        else:
            subtr_dir=[0 if sim.sequence_[-1][i]==0 else -1 for i in range(self.m)]
        subtractions=subsets_nonempty(subtr_dir, np.Inf)
        # creates all nonempty subsets of directions in which we may subtract
        for sub in subtractions:
            sub=minus_one_to_zero(sub)
            new_vert=sim.vertex_-np.array(sub)
            for i in range(self.m):
                if new_vert[i]==-1:
                    new_vert[i]=self.shape[i]-1
            sequence_new=[sub if i==0 else list(np.array(sim.sequence_[i-1])+np.array(sub)) for i in range(sim.dim+1)]
            output.append((simplex(np.array(new_vert), sequence_new),0))
        return output
        

    def cube_twice_barycenter(self,cube):
        out=[0]*self.m
        for i in range(self.m):
            out[self.m-i-1]=cube//self.offsets[self.m-i-1]
            cube=cube % self.offsets[self.m-i-1]
        return out

    def cube_vertex(self,cube): # returns the lower left vertex as an array
        tb=self.cube_twice_barycenter(cube)
        return np.array(tb)//2

    def cube_span(self,cube):
        return np.array(self.cube_twice_barycenter(cube))%2

    def cube_dim(self,cube):
        return np.count_nonzero(self.cube_span(cube))

    def module(self,arr):
        return np.array([arr[i] % self.shape[i] for i in range(self.m)])
        
    def cube_vertices(self,cube):
        vertex=self.cube_vertex(cube) # np array
        span=self.cube_span(cube)
        args=[range(span[i]+1) for i in range(self.m)]
        prod=list(product(*args))
        return ([self.module(vertex+np.array(item)) for item in prod])

    def cubical_boundary(self,cube):
        assert self.cube_in_sc(cube), 'cube not in the cubical complex'
        twice_bar=self.cube_twice_barycenter(cube)
        out,s =[], 1
        for i in range(self.m):
            if twice_bar[i] & 1:
                if twice_bar[i]>2*(self.shape[i]-1):
                    out.append((cube+self.offsets[i]-self.offsets[i+1], s))
                else:
                    out.append((cube+self.offsets[i], s))
                out.append((cube-self.offsets[i], -s))
                s=-s
        return out
        
    def cubical_coboundary(self,cube):
        # returns the list of +1-dimensional super-cubes
        assert self.cube_in_sc(cube), 'cube not in the cubical complex'
        twice_bar=self.cube_twice_barycenter(cube)
        out,s =[], 1
        for i in range(self.m):
            if twice_bar[i] & 1: # ith coordinate is odd
                s=-s
            else:
                out.append((cube+self.offsets[i], -s))
                if twice_bar[i]//2 > 0:
                    out.append((cube-self.offsets[i], s))
                else:
                    out.append((cube-self.offsets[i]+self.offsets[i+1], s))
        return out

    def cube_face(self,cube,i,sign): #returns the ith,sign-face of the Cube, when sign is zero then returns copy of self
        assert self.cube_in_sc(cube), 'cube not in the cubical complex'
        assert sign==1 or sign==-1 or sign==0 
        if sign==0: return cube
        twice_bar=self.cube_twice_barycenter(cube)
        ind=0
        for j in range(self.m):
            if (twice_bar[j] & 1): # jth coordinate is odd
                if i==ind:
                    if twice_bar[j]>2*(self.shape[j]-1) and sign==1:
                        return cube+self.offsets[j]-self.offsets[j+1]
                    return cube+sign*self.offsets[j]
                ind+=1
        assert False # what are we doing here?

    def k_simplices_list(self,k): # probably unused
    # returns a list of k-simplces
        output=list()
        vertices=self.vertices()
        seq=sequences([0]*self.m,k)
        if k==0: # vertices
            for vert1 in vertices:
                output.append(simplex(np.array(vert1), []))
        else: # k>0
            for vert1 in vertices:
                for s in seq:
                    output.append(simplex(np.array(vert1), s)) 
        return output
                    
    def vertex2cube(self,vertex):
        return sum([2*vertex[i]*self.offsets[i] for i in range(self.m)])
    
    def twice_baryc2cube(self,twice_bar):
        return sum([twice_bar[i]*self.offsets[i] for i in range(self.m)])        
    
    def zero_cubes(self): # returns the list of 0-cube ids's and list of forbidden (positive) directions
        const=[2]
        for i in range(1,self.m):
            const.append(const[i-1]+self.offsets[i])
        def next_item(vertex,ids):
            for i in range(self.m): # i goes from m-1 to 0
                if vertex[i] < (self.shape[i]-1):
                    vertex[i]=vertex[i]+1
                    return vertex, ids+const[i], [vertex[i]<self.shape[i]-1 for i in range(self.m)]
                else:
                    vertex[i]=0
            return None,None,None

        vert,forb_pos=[],[]
        v,ids,f_pos=[0]*self.m,0,[True]*self.m
        while v != None:
            vert.append(ids)
            forb_pos.append(f_pos)
            v,ids,f_pos=next_item(v,ids)
        return vert,forb_pos

    def zero_cubes_old(self): #older version: equivalent to zero_cubes but slower
        out=[]
        for v in self.vertices():
            out.append(self.vertex2cube(v))
        return out
        
    def k_cube_list(self,vert_ids,forb_dir,k): # ? maybe unused 
        if k==0:                # forb_dir is unused here
            return vert_ids
        out=list()
        comb=list(combinations(range(self.m),k))
        for i in range(len(vert_ids)):
            for c in comb:
                v=vert_ids[i]
                out.append(v+sum([self.offsets[c[j]] for j in range(k)]))
        return out
                
    def coboundary_matrix(self,vert_ids,forb,k):
        k_cubes=self.k_cube_list(vert_ids,forb,k)
        k_cubes_inv={}
        for i in range(len(k_cubes)):
            k_cubes_inv[k_cubes[i]]=i
        # dictionary k_cubes_inv created
        
        kp1_cubes=[]
        matrix=[[] for i in range(len(k_cubes))] # empty matrix
        comb=list(combinations(range(self.m),k+1))
        for i in range(len(vert_ids)):
            for c in comb:
                v=vert_ids[i]
                new_cube=v+sum([self.offsets[c[j]] for j in range(k+1)])
                kp1_cubes.append(new_cube)
                s=1
                for j in range(k+1):
                    if forb[i][c[j]]:# if the c[j] th direction is on the 'positive' boundary
                        cub=new_cube+self.offsets[c[j]]
                        matrix[k_cubes_inv[cub]].append((new_cube,s))
                    else:
                        cub=new_cube+self.offsets[c[j]]-self.offsets[c[j]+1]
                        matrix[k_cubes_inv[cub]].append((new_cube,s))
                    cub=new_cube-self.offsets[c[j]]
                    matrix[k_cubes_inv[cub]].append((new_cube,-s))
                    s=-s
        return matrix, k_cubes,k_cubes_inv
        
    def simplices_of_cube(self,cub): # returns a list of pairs (sim,sign) 
                            # where sim ranges over all top-dim. simplices
        def change_minus_ones_to_zeros(lll):
        # it each item l of the list ll of the list lll, it changes -1 to 0
            for i in range(len(lll)):
                for j in range(len(lll[i])):
                    seq_new=[0 if x==-1 else x for x in lll[i][j]]
                    lll[i][j]=seq_new
            return lll

        twice_bar=self.cube_twice_barycenter(cub)
        output=[]
        vert=np.array(twice_bar)//2
        cube_dim=np.count_nonzero(np.array(twice_bar)-2*vert)
        if cube_dim==0:
            return simplex(vert,[]),1
        nontriv_dim=[0 if (twice_bar[i] & 1) else -1 for i in range(self.m)]
        seq=sequences(nontriv_dim,cube_dim)
        seq=change_minus_ones_to_zeros(seq)
        for s in seq:
            new_sim=simplex(vert,s)
            output.append((new_sim, new_sim.orientation()))
        return output

    def smallest_supercube(self,sim):
        twice_bar=sim.vertex_first()+sim.vertex_last()
        assert([twice_bar[i]<2*self.shape[i] for i in range(self.m)]==[True]*self.m)
        return self.twice_baryc2cube(list(twice_bar))

    def k_simplices_of_cube(self,cub,k):
        def change_minus_ones_to_zeros(lll):
        # it each item l of the list ll of the list lll, it changes -1 to 0
            for i in range(len(lll)):
                for j in range(len(lll[i])):
                    seq_new=[0 if x==-1 else x for x in lll[i][j]]
                    lll[i][j]=seq_new
            return lll

        output=[]
        cube_dim=self.cube_dim(cub)
        vert=self.cube_vertex(cub)
        twice_bar=self.cube_twice_barycenter(cub)
        assert(k<=cube_dim), "dimension of cube smaller than k"
        if cube_dim==0:
            return simplex(np.array(sc.cube_vertex()), [])
        nontriv_dim=[0 if (twice_bar[i] & 1) else -1 for i in range(self.m)]
        # first simplices with vertex=cube.vertex
        for s in change_minus_ones_to_zeros(sequences(nontriv_dim,k)):
            new_sim=simplex(vert,s)
            output.append(new_sim)
            #v=vert+np.array([max(x,0) for x in subs])
        # then other simplices
        for s in change_minus_ones_to_zeros(sequences(nontriv_dim,k+1)):
            v=self.module(vert+np.array(s[0]))
            seq=[list(s[i]-np.array(s[0])) for i in range(1,k+1)]
            new_sim=simplex(v,seq)
            output.append(new_sim)
        return output

    def cubical_star(self,cube):
        def create_products(i,tb):
            if tb[i] % 2==1:
                return [0]
            elif tb[i] == 0:
                return [1,2*self.shape[i]-1]
            else:
                return [-1,1]
        assert(self.cube_in_sc(cube))
        tb=self.cube_twice_barycenter(cube)
        output=[]
        args=[create_products(i,tb) for i in range(self.m)]
        for s in product(*args):
            output.append(cube+sum([self.offsets[i]*s[i] for i in range(self.m)]))
        return output
