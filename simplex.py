# simplex is represented as a pair (vertex, sequence)
# vertex is an array, sequence a list of lists of zeros/ones
# the sequence is assumed to be increasing
# the ith vertex of the simplex is vertex+sequence[i]
# example: ([2,3], [[1,0], [0,1]]) represents the triangle (2,3), (3,3), (3,4)
import numpy as np
from sequences import dict_of_sequences,sequences

class simplex:
    def __init__(self,vertex_,sequence_):
        self.vertex_ = vertex_
        self.sequence_=sequence_
        self.dim = len(sequence_)
        self.dim_ambient=len(vertex_)

#    def __str__(self):
#        return self.__repr__()
        
    def __repr__(self):
        return "simplex("+str(self.vertex_)+","+str(self.sequence_)+")"
        
    def __eq__(self, another):
        return np.array_equal(self.vertex_,another.vertex_) and self.sequence_  == another.sequence_

    def __hash__(self):
        return hash(self.convert_to_tuples()) #hash((self.vertex_, self.sequence_))

    def vertex(self,k): # returns an array
        if k==0:
            return self.vertex_
        else:
            return self.vertex_+np.array(self.sequence_[k-1])
            # + is addition of arrays, not concatenation

    def vertex_first(self):
        return self.vertex(0)
    
    def vertex_last(self):
        return self.vertex(self.dim)

    def orientation(self):
    # returns the orientation of the simplex in R^n
    # this is unoptimized and could be improved by replacing counting inverses by counting cycles

        def first_is_larger(vec1,vec2):
            #assert(len(vec1)==len(vec2))
            for i in range(len(vec1)):
                if vec1[i]>vec2[i]:
                    return 1
                elif vec1[i]<vec2[i]:
                    return -1
            return 0 # if tup1==tup2

        # it is a vertex:
        if self.dim==0: 
            return 1
        
        # otherwise:
        diff=list()
        diff.append(self.sequence_[0])
        for i in range(1,self.dim):
            # vector of differences:
            diff.append(np.array(self.sequence_[i])-np.array(self.sequence_[i-1]))
        perm_sign=1
        for i in range(self.dim):
            for j in range(i+1,self.dim):
                # we count the number of permutations
                perm_sign=perm_sign*first_is_larger(diff[i],diff[j])
        return perm_sign

    def boundary(self):
    # returns a list of pairs (simplex, orientation)        
        if self.dim==0:
            return [] # boundary of a point is the empty set
        output=list()
        # first item will be the simplex with deleted first vertex
        vert_0=self.vertex_+np.array(self.sequence_[0])
        seq_new=[list(np.array(self.sequence_[i+1])-np.array(self.sequence_[0])) for i in range(self.dim-1)]
        output.append((simplex(vertex_=vert_0, sequence_=seq_new), 1)) # first boundary
        # the remaining items -- we just delete one element of sequence_
        for i in range(self.dim):
            seq_new=[]
            for j in range(self.dim-1):
                if j<i:
                    seq_new.append(self.sequence_[j])
                else:
                    seq_new.append(self.sequence_[j+1])
            output.append((simplex(self.vertex_, seq_new), (-1)**(i+1)))
        return output

    def print_sim(self):
        return self.vertex_, self.sequence_
        
    def convert_to_tuples(self):
        return (tuple(self.vertex_), tuple(tuple(item) for item in self.sequence_))
        
    def split_sim(self,k):
    # returns two simplices: [v0.,..., vk] and [vk,....,vn]
        seq1=self.sequence_[0:k]
        vert2=self.vertex(k)
        if k==0:
            seq2=self.sequence_
        else:
            seq2=[list(np.array(self.sequence_[x])-np.array(self.sequence_[k-1])) for x in range(k,self.dim)]
        return (simplex(self.vertex_,seq1), simplex(np.array(vert2), seq2))

