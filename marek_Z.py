import copy
import random
import cProfile
import numpy as np
from time import *

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import pstats
import operator

DEBUG = False
INFO = True


def add_mod_p(R, L, x, p):
    """Adds two chains mod p. More precisely returns a list of pairs of
    indices and coefficients of the chain L*x + R mod p.
    It is also inefficient by design, but better than before.
    """

    if isinstance(L, dict):
        elems = L.items()
    else:
        elems = L

    for index, coeff in elems:
        # assert coeff
        if index in R:
            new_coeff = (R[index] + x * coeff) % p
            if new_coeff:
                R[index] = new_coeff
            else:
                del R[index]
        else:
            new_coeff = (x * coeff) % p
            if new_coeff:
                R[index] = new_coeff


def add_over_Z(R, L, rx, lx):
    """Adds two chains. More precisely returns a list of pairs of
    indices and coefficients of the chain L*x + R*y. 
    It is also inefficient by design, but better than before.
    """
    if isinstance(L, dict):
        elems = L.items()
    else:
        elems = L

    if rx == 0:
        R.clear()
    elif rx != 1:
        # this can be rewritten as an R.update call, possibly faster
        #for index in R.iterkeys():
        for index in R.keys():
            R[index] *= rx
            assert abs(R[index]) < 100, "index too big: R[index]="+str(R[index])
    

    # if DEBUG: print "adding: (xL+yR) {lx}{L} + {rx}{R}".format(lx=lx, rx=rx, L=L, R=R)

    # this can be rewritten as an R.update call, possibly faster, but would leave some zeros...
    for index, coeff in elems:
        # assert coeff
        if index in R:
            new_coeff = R[index] + lx * coeff
            if new_coeff:
                R[index] = new_coeff
            else:
                del R[index]
        else:
            new_coeff = lx * coeff
            if new_coeff:
                R[index] = new_coeff

        assert abs(new_coeff) < 100, "index too big: new_coeff="+str(new_coeff)

    # if DEBUG: print "got {}".format(R)


def inverse_mod_p(a, p):
    assert (a + p) > 0
    if not hasattr(inverse_mod_p, "p") or inverse_mod_p.p != p:
        inverse_mod_p.p = p
        inverse_mod_p.inverses_mod_p = tabulate_inverses(p)

    return inverse_mod_p.inverses_mod_p[(a + p) % p]


def congruence(L, R, p):
    """Computes x such that L*x + R = 0 mod p, where L, R are integers."""
    # this way we can have a 'static' array of inverses
    # no need to pass it to this function etc.
    assert p > 0
    # assert (L%p == 1 or L%p == -1%p), "L more than one"

    x = (((-R + p) % p) * inverse_mod_p(L, p)) % p

    assert (L * x + R) % p == 0
    return x


def extended_congruence(L, R):
    """Solves the reduction equation for SNF over Z"""

    assert L != 0
    if R % L == 0:
        a, b, c, d = 1, 0, -R//L, 1
        assert c * L + d * R == 0
        return a, b, c, d

    oldL, oldR = L, R
    a, b, c, d = 1, 0, 0, 1  # id
    while R != 0:
        q, r = L // R, L % R
        a -= q * c
        b -= q * d
        L, R = R, r
        a, b, c, d = c, d, a, b

    assert c * oldL + d * oldR == 0  # R in the new basis is zero
    if d < 0:
        c, d = -c, -d
    assert c * oldL + d * oldR == 0  # R in the new basis is zero

    return a, b, c, d  # CL' = a * CL + b * CR; CR' = c * CL +  CR for original *columns*


def tabulate_inverses(p):
    """Computes a table of inverses mod p by brute force. Since p << n
    this is fast enough.
    """
    if INFO: print("tabulating inverses mod {p} -- you should see this ONCE (per p)".format(p=p))
    I = [None] * p
    for a in range(p):
        for b in range(p):
            if (a * b) % p == 1:
                I[a] = b
    return I


def reduce_one_column(M, C, i, curr, curr_change, low_index, p):
    if p == 0:
        return reduce_one_column_Z(M, C, i, curr, curr_change, low_index)

    lowest = lowest_one(curr)

    if curr_change is not None:
        curr_change = dict(curr_change)
    curr = dict(curr)

    while low_index[lowest] != -1:  # small hack: low_index[-1]===-1
        next_index = low_index[lowest]
        colliding = M[next_index]

        x = congruence(lowest_one_coef(colliding), lowest_one_coef(curr), p)
        add_mod_p(curr, colliding, x, p)
        lowest = lowest_one(curr)

        if curr_change is not None:
            add_mod_p(curr_change, C[next_index], x, p)

    return curr, curr_change


def reduce_one_column_Z(M, C, i, curr, curr_change, low_index):
    lowest = lowest_one(curr)
    curr = dict(curr)

    if curr_change is not None:
        curr_change = dict(curr_change)

    while low_index[lowest] != -1:  # small hack: low_index[-1]===-1
        next_index = low_index[lowest]
        colliding_list = M[next_index]

        a, b, c, d = extended_congruence(lowest_one_coef(colliding_list), lowest_one_coef(curr))

        # If reducing the RHS of the equation, we require divisibility
        if i is None and d != 1:
            return curr, curr_change

        if i is not None and (a != 1 or b != 0):
            if DEBUG: print("Altering the *colliding* column: {next}".format(next=next_index))
            colliding = copy.copy(colliding_list)
            add_over_Z(R=colliding, L=curr, lx=b, rx=a)
            M[next_index] = colliding

        if DEBUG: print("Reducing {i}, coll with ind {next}".format(i=i, next=next_index))
        add_over_Z(R=curr, L=colliding_list, lx=c, rx=d)

        lowest = lowest_one(curr)

        if curr_change is None:
            continue

        colliding_change_list = copy.copy(C[next_index])  # ?
        if i is not None and (a != 1 or b != 0):  # this was crucial for performance
            colliding_change = dict(colliding_change_list)
            if DEBUG: print("Altering the *colliding* basis: {next}".format(next=next_index))
            add_over_Z(R=colliding_change, L=curr_change, lx=b, rx=a)
            C[next_index] = colliding_change

        if DEBUG: print("Basis change {i}, coll with ind {next}".format(i=i, next=next_index))
        add_over_Z(R=curr_change, L=colliding_change_list, lx=c, rx=d)

    if i is not None:
        M[i] = curr
        if curr_change is not None:
            C[i] = curr_change

    return curr, curr_change


def lowest_one(c):
    if not c:
        return -1
    if isinstance(c, dict):
        return max(c)
    return max(c)[0]

def lowest_one_coef(c):
    if not c:
        return 0
    if isinstance(c, dict):
        return c[lowest_one(c)]
    return max(c)[1]


def persistence_of_cycle(M, c, p=2, change_basis=True, total_RHS_reduction = False):
    """Computes the persistence of a cycle c, given a boundary matrix M.
    More precisely, it returns the chain which kills the cycle,
    i.e. the indices  of the columns of boundary matrix which
    add up to this cycle mod p, and the index persistence of this cycle,
    i.e. the index of the chain which kills it.
    If change_basis == False, the change of basis boundary is not maintained,
    and the returned chain is given in the basis after reduction.
    """
    if not M and not c:
        return {}, -1

    mx_ind = int(max(lowest_one(col) for col in M))
    low_index = [-1] * (mx_ind + 2)
    n = len(M)

    C = [[(i, 1)] for i in range(n)]  # change of basis
    cob = dict()

    #if INFO: print "looking for: ", c

    if INFO: print("reducing matrix of n columns: {0}".format(n))
    each = int(n ** 0.75)

    for i in range(n):
        curr = M[i][:]
        curr_change = C[i] if change_basis else None

        if (i + 1) % each == 0:
            if INFO: print("done: {i}/{n}={r}%".format(r=round(100.0 * i / n, 2), i=i, n=n))

        curr, curr_change = reduce_one_column(M, C, i, curr, curr_change, low_index, p)

        M[i] = curr
        C[i] = curr_change
        #print "in Change of basis... ", max(C[i].iteritems(), key=operator.itemgetter(1))[1]


        if curr:
            lowest = lowest_one(curr)
            if change_basis:
                C[i] = curr_change  # COB only for nonzero columns
            low_index[lowest] = i

        if low_index[lowest_one(c)] != -1:
            c, cob = reduce_one_column(M, C, None, c, cob, low_index, p)

        if not c:
            if INFO: print(
            "finished after reducing  {i}/{n} ({p}%) initial columns".format(i=i, n=n, p=round(100.0 * i / n, 2)))
            break  # we got what we wanted

    if c and total_RHS_reduction:
        remaining = []
        while c:
            if low_index[lowest_one(c)] == -1:
                remaining.append((lowest_one(c), c[lowest_one(c)]))
                del c[lowest_one(c)]
            else:
                c, cob = reduce_one_column(M, C, None, c, cob, low_index, p)
        print("returning the remaining elements in RHS")
        return remaining      
        
    if c:
        raise StandardError("No solution found!")

    return cob, lowest_one(cob)


def parse_chain(s):
    return [tuple(reversed([(int(x)) for x in x.split('*')])) for x in s.strip().split(' ')]


def normalized_chain_mod_p(c, p):
    if p == 0:
        return c
    return [(x[0], (x[1] + p) % p) for x in c]


def filtration_values(LS):
    import re
    rgx = re.compile(".+\[(\d+(\.?\d+)?[,-]\d+(\.\d+)?)")

    is_filt = lambda line: rgx.match(line)
    parse_filt = lambda line: (rgx.match(line).group(1))

    f = 0
    F = [0] * len(LS)  # let's say...
    k = 0
    for x in LS:
        if is_filt(x):
            f = parse_filt(x)
        elif not x.startswith('#'):
            F[k] = f
            k += 1

    return F


def verify_A_x_equals_b_mod_p(A, x, b, p):
    if INFO: print("verifying mod p...")
    s = dict(b)
    for i, coef in x.items():
        add_mod_p(s, A[i], coef, p)

    assert not s
    if INFO: print("verified the equation correctly (but not the persistence)!")


def verify_A_x_equals_b(A, x, b):
    if INFO: print("verifying...")
    s = dict(b)
    for i, coef in x.items():
        add_over_Z(R=s, L=A[i], lx=coef, rx=1)

    assert not s

    if INFO: print("verified the equation correctly (but not the persistence)!")


def solve(filepath, P, change_basis=True):
    """Parses the given file and computes the persistence of the given cycle.    
    """

    with open(filepath, 'r') as f:
        LS = f.readlines()
        F = filtration_values(LS[2:])
        data = [line for line in LS if not line.startswith('#')]

        c = parse_chain(data[0])
        c = normalized_chain_mod_p(c, P)

        assert c  # c should be nonempty
        M = [normalized_chain_mod_p(parse_chain(x), P) for x in data[1:]]

        oldM = copy.deepcopy(M) if change_basis else M

        if not change_basis:
            print("warning: the change of basis matrix will not be maintained!")

        ch, mx = persistence_of_cycle(M, c, P, change_basis)

        print("computed for file: " + filepath)
        print("index persistence treshold of your cycle is {0}".format(mx))
        print("persistence treshold of your cycle is {0}".format(F[mx]))
        print("we have a suspect he killed your cycle! he's too ugly to display but we can tell you his length is {0}...".format(
            len(ch)))

        print("wait till we verify!")

        if len(ch) < 10:
            print("chain: " + str(ch))
            print("cycle wanted: " + str(c))

        if P != 0:
            verify_A_x_equals_b_mod_p(oldM, ch, c, P)
        else:
            verify_A_x_equals_b(oldM, ch, c)


def main():
    import timeit
    import sys
    import cProfile, pstats
    if len(sys.argv) <= 1:
        print("input: filepath [p] [no_basis_change]; will exit now")
        return 0

    filepath = sys.argv[1]

    p = int(sys.argv[2]) if len(sys.argv) >= 3 else 2
    change_basis = 'no_basis_change' not in sys.argv[1:]

    what = lambda: solve(filepath, p, change_basis)
    pr = cProfile.Profile()
    pr.enable()
    t = timeit.timeit(what, number=1)

    pr.disable()
    s = StringIO.StringIO()
    sortby = 'tottime'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    print("\n\nPROFILING INFORMATION WITH cProfile:")
    print('\n'.join(s.getvalue().split("\n")[0:10]))
    print("it took {0} seconds.\n\n".format(t))

def reduction_sanity_check():
    P = 0

    if P == 0:
        COEFFS = [c for c in range(-1, 2) if c]  # Without zero.
    else:
        COEFFS = range(1, P)

    s = 1767
    #s = random.randint(1, 10000)
    print(s)
    random.seed(s)

    def random_col(n, k):
        indices = sorted(set(random.randint(0, n - 1) for _ in range(k)))
        coeffs = (random.choice(COEFFS) for _ in range(len(indices)))
        return zip(indices, coeffs)

    def random_instance(n, k=4, m=4):
        M = [random_col(n, k) for _ in range(n)]

        X = set(random_col(n, m))

        print(len(X))
        C = dict()
        for i, x in X:
            if P == 0:
                add_over_Z(C, M[i], 1, x)
            else:
                add_mod_p(C, M[i], x, P)

        return M, X, C

    def test_instance(M, X, c):
        if DEBUG:
            for m in M:
                print(m)
            print("")
            print(c)

        M0 = copy.deepcopy(M)

        pr = cProfile.Profile()
        pr.enable()

        try:
            print(c)
            TRY_TO_CHEAT = False

            if TRY_TO_CHEAT:
                p = 997
                c2 = copy.copy(c)
                M2 = copy.deepcopy(M)
                for x in c2:
                    c2[x] = (c2[x] + p) % p

            ch = persistence_of_cycle(M, c, P)[0]

            print(ch)

            if TRY_TO_CHEAT:
                ch_mod = persistence_of_cycle(M2, c2, p)[0]
                verify_A_x_equals_b_mod_p(M0, ch_mod, c2, p)
                print(sorted(ch.items()), "\n", sorted(ch_mod.items()))
                for x in ch_mod:
                    if ch_mod[x] > p/2:
                        ch_mod[x] = ch_mod[x] - p

                print("after fixing:")
                print(sorted(ch.items()), "\n", sorted(ch_mod.items()))

                verify_A_x_equals_b(M0, ch_mod, c) # over Z

            if P != 0:
                verify_A_x_equals_b_mod_p(M0, ch, c, P)
            else:
                verify_A_x_equals_b(M0, ch, c)

            for x in ch:
                ch[x] *= -1

            if sorted(X) != sorted(ch.items()):
                print(sorted(X), "\n", sorted(ch.items()))
                throw
                'we have probably found a smaller persistence sol. than the intended, but please verify!'

        except StandardError:
            print("ERROR!!!")
            print("old:\n{}\n red:\n{}\n cycle:{}\n chain:{}".format(M0, M, c, X))
            raise
            exit(1)

        pr.disable()
        s = StringIO.StringIO()
        sortby = 'tottime'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print("\n\nPROFILING INFORMATION WITH cProfile:")
        print('\n'.join(s.getvalue().split("\n")[0:10]))

    for i in range(10000):
        test_instance(*random_instance(32, 15, 5))
        print(i)

#sanity_test()
from collections import defaultdict

def get_chunks(M, values):
    CH = defaultdict(list)
    for i, (c,v) in enumerate(zip(M, values)):
        CH[v].append((i,c))
    return CH

def update_low_index(reduced, i, index):
    if reduced:
        index[lowest_one(reduced)] = i    

"""
for each filtration value in U:
    reduce each column in chunk:
    record the killers with coefficients

for each column in L with:    
    if he's NOT killed immidiately:
        add him to generators
"""
def persistent_cycles(L, U, valuesL, valuesU):
    mx_indL = int(max(lowest_one(col) for col in L))
    mx_indU = int(max(lowest_one(col) for col in U))
    
    low_indexL = [-1] * (mx_indL + 2)
    low_indexU = [-1] * (mx_indU + 2)
    n = len(L)
    CL = [[(i, 1)] for i in range(n)] # change of basis

    killers = defaultdict(lambda: defaultdict(tuple)) # val : {lowest_index : lowest_coef}   
    chunksU = sorted(get_chunks(U, valuesU).items())
    RU = {}
    for val, chunk in chunksU:    
        for i, col in chunk:            
            reduce_one_column_Z(RU, None, i, col, None, low_indexU)            
            update_low_index(RU[i], i, low_indexU)
            #if i%10000 == 0: 
            #   with open("debugfile", "a") as df: df.write("RU["+str(i)+"]: "+str(RU[i])+"\n")
        for i, col in chunk:
            reduced = RU[i]
            if not reduced:
                continue            
            killers[val][lowest_one(reduced)] = lowest_one_coef(reduced)            

    G = []
    RL = {}
    miu = {}

    if DEBUG: print("killers:", killers.items())
    #with open("debugfile",'a') as df:
    #    df.write("   finished the first part (killers) \n")
    #    df.write("   time: "+str(ctime())+"\n")

    for i, col in enumerate(L):
        if DEBUG: print("c:", i,col)
        val = valuesL[i]
        reduce_one_column_Z(RL, CL, i, col, CL[i], low_indexL)
        reduced = RL[i]
        update_low_index(RL[i], i, low_indexL)        
        if not reduced: # cycle            
            generator = CL[i]
            if DEBUG: print("check:", val, reduced, generator)
            if i not in killers[val] or lowest_one_coef(generator) % killers[val][i] != 0:
                G.append(generator)
                if DEBUG: print("ok")
                #with open("debugfile",'a') as df:
                #   df.write("    new generator of size "+str(len(generator))+"\n") 
                #   #df.write("    generator: "+str(generator)+'\n')
                #   df.write("       max val: "+str(max(np.abs(list(generator.values()))))+'\n')
                #   df.write("       filtration: "+str(max([valuesL[item] for item in generator.keys()]))+"\n")
        miu[i] = len(G)
                
    return G, miu

def add_coefficients(M):
    for i,c in enumerate(M):
        M[i] = zip(M[i], [1] * len(M[i]))    

def cycles_sanity_check():
    L = [[0,1], [0,1], [1], [2], [2], [3]]
    vL = [0,1,2,3,4,5]
    add_coefficients(L)
    U = [[4]]
    vU = [4]
    add_coefficients(U)

    print(L)
    print(persistent_cycles(L, U, vL, vU))

#cycles_sanity_check()        

if __name__ == "__main__":
    from hom_algebra import *
    #matrix1=np.load("mat1_small.npy")
    #matrix2=np.load("mat2_small.npy")
    #list1=np.load("l1_small.npy")
    #list2=np.load("l2_small.npy")
    matrix1=np.load("matrix1.npy")
    matrix2=np.load("matrix2.npy")
    list1=np.load("list1.npy")
    list2=np.load("list2.npy")
    with open('debugfile', 'a') as df:
       df.write("nr of col matrix1::"+str(len(matrix1))+"\n")
       df.write("nr of col matrix2::"+str(len(matrix2))+"\n")
       df.write("list1 last: "+str(list1[-1])+"\n")
       df.write("list2 last: "+str(list2[-1])+"\n")
       df.write("reformatting matrices... \n")
    matrix1=[list(dict(item).items()) for item in matrix1]
    matrix2=[list(dict(item).items()) for item in matrix2]
    with open('debugfile', 'a') as df:
       df.write("matrix1[0]:"+str(matrix1[0])+"\n")
       df.write("matrix2[0]:"+str(matrix2[0])+"\n")
       df.write("list1[0:10]:"+str(list1[0:10])+"\n")
       df.write("list2[0:10]:"+str(list2[0:10])+"\n")
       df.write("matrix1 times matrix2[0:100]: "+str(matrix_times_matrix(matrix1, matrix2[0:100]))+"\n")
       df.write("loaded files, running persistent_cycles...\n")
    G,miu=persistent_cycles(matrix1, matrix2, list1, list2)
    np.save("G", G)
    np.save("miu", miu) 
    with open('debugfile', 'a') as df:
       df.write("saved and finished\n")
