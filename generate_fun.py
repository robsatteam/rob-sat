#import pylab
#import matplotlib.pyplot as plt
import re
import numpy as np
from numpy import Inf
import ctypes
from sequences import dict_of_sequences
from itertools import product
from cubical_sc import *
from cubical_sc_torus import *

def norm(vert):
    #return np.linalg.norm(vert) # Euclidean
    return np.max(np.abs(vert))  # maxnorm

# generates the simplicial approximation (func_sim) and array of norms (func_val_abs)
def func_values(sc,func_arr,n,rounding,tor):
    def simplicial_map(vector): # given f(v), it returns  f'(v)\in {-n,...,-1,1,2,...,n}
        ind=np.argmax(np.abs(vector))
        return int((ind+1)*np.sign(vector[ind]))

    shape_ext=[sc.shape[i]+1 for i in range(sc.m)]
    func_val_abs=np.zeros(shape_ext)
    func_sim=np.zeros(shape_ext, dtype='int')
    for vert in sc.vertices():
        func_value=func_arr[tuple(vert)]
        func_sim[tuple(vert)]=simplicial_map(func_value)
        func_val_abs[tuple(vert)]=round(norm(func_value),rounding)

    # artificial function values, "outside the grid":
    for i in range(sc.m):
        args=[range(sc.shape[j]+1) if j!=i else [sc.shape[i]] for j in range(sc.m)]
        for vert in product(*args):
            if tor:
                vert_identical=[vert[j] if vert[j]<sc.shape[j] else 0 for j in range(sc.m)]
                func_val_abs[tuple(vert)]=func_val_abs[tuple(vert_identical)]
                func_sim[tuple(vert)]=func_sim[tuple(vert_identical)]
            else:
                func_val_abs[tuple(vert)]=-Inf
                func_sim[tuple(vert)]=0
    return func_val_abs, func_sim

def approximate_alpha(sc,func_array): # func is assumed to be a tensor here
    out=-1.0
    edges=sc.k_simplices_list(1)
    for edge in edges:
        vert1=edge.vertex(0)
        vert2=edge.vertex(1)
        val1=func_array[sc.module(vert1)]
        val2=func_array[sc.module(vert2)]
        diff=norm(np.array(val1)-np.array(val2))
        out=max(out,diff)
    return out

def generate_gamma_gaussian(shape,l,tor):
    # generates an approximation of the square root of the FFT of the Gaussian function
    if tor: shape_ext=np.array(shape)
    else: shape_ext=2*np.array(shape)
    C=np.zeros(shape_ext)
    it=np.nditer(C, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        diff=[min(it.multi_index[i]*2.0/shape_ext[i], 2.0-it.multi_index[i]*2.0/shape_ext[i]) for i in range(len(shape))]
        diff_n=np.linalg.norm(np.array(diff))
        it[0]=np.exp(-diff_n**2/(2*l**2))
        it.iternext()
    gamma=np.fft.fftn(C)
    gamma=np.real(gamma)
    if np.min(gamma)<-1.0: 
        print("Warning: generating random GF doesn't go well (min gamma=", np.min(gamma), ")")
    #print "min / max of gamma:", np.min(gamma), np.max(gamma)
    it=np.nditer(gamma, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        #assert(it[0]>-tolerance)
        it[0]=max(it[0],0.0)
        it.iternext()
    return np.sqrt(gamma)


def generate_gamma_polydecrease(shape,p,tor):
    # returns the square root of the (normalized) field 1/(1+|x|^2)^p
    if tor: shape_ext=np.array(shape)
    else: shape_ext=2*np.array(shape)
    gamma=np.zeros(shape_ext)
    for item in product(*[range(shape_ext[i]) for i in range(len(shape_ext))]):
        diff=[min(item[i], shape_ext[i]-item[i]) for i in range(len(shape_ext))]
        diff_n=np.linalg.norm(diff)
        gamma[item]=1/((1+diff_n**2)**p)
    const=np.prod(shape_ext)/np.ndarray.sum(gamma)
    for item in product(*[range(shape_ext[i]) for i in range(len(shape_ext))]):
        gamma[item]*=const
    return np.sqrt(gamma)

def GP(shape,sqrt_gamma,tor,param=""):
    # generates the Gaussian Process FT^{-1}(FT(R) sqrt_gamma) where R is normal iid
    # The resulting process has covariance gamma (square of sqrt_gamma)
    if tor: shape_ext=np.array(shape)
    else: shape_ext=2*np.array(shape)
    random=np.zeros(shape_ext)
    for item in product(*[range(shape_ext[i]) for i in range(len(shape_ext))]):
        random[item]=np.random.normal(0,1)
    random_ft=np.fft.fftn(random)
    prod=sqrt_gamma*random_ft
    output=np.fft.ifftn(prod)
    output=np.real(output)   
    if tor: # torus
        if param=="centered":
            midpoint=np.array(shape)//2
            midvalue=output[tuple(midpoint)]
            for item in product(*[range(shape[i]) for i in range(len(shape))]):
                output[item]-=midvalue
            return output
        else:
            return output
    # catting the first quadrant from the output (non-torus version)
    out_cat=np.zeros(shape)
    midpoint=np.array(shape)//2
    midvalue=output[tuple(midpoint)]
    if param=="centered":
        for item in product(*[range(shape[i]) for i in range(len(shape))]):
            out_cat[item]=output[item]-midvalue
    else:
        for item in product(*[range(shape[i]) for i in range(len(shape))]):
            out_cat[item]=output[item]
    return out_cat

def Gaussian_Field_gamma(shape,n,sqrt_gamma,tor,param=""):
    # generates n independent Gaussian Processes as components of a vector field
    out=np.zeros(shape+[n])
    for i in range(n):
        out[:,...,:,i]=GP(shape,sqrt_gamma,tor,param)
    return out

# m,n=4,3
def func_rand_quadratic_43(sc,rounding):
    def simplicial_map(vector): # given f(v), it returns  f'(v)\in {-n,...,-1,1,2,...,n}
        ind=np.argmax(np.abs(vector))
        return int((ind+1)*np.sign(vector[ind]))

    def func_rand_quadratic_scalar(sc,rounding):
        f_val=np.zeros(sc.shape)
        midpoint=(np.array(sc.shape)-np.ones(sc.m))/2.0
        ra=np.random.normal(0,1,10)
        def func_val(ra,vert):
            vert=[2.0*float((vert[i]-midpoint[i]))/(sc.shape[i]-1) for i in range(sc.m)]
            x0,x1,x2,x3=vert
            return ra[0]*x0*x0+ra[1]*x1*x1+ra[2]*x2*x2+ra[3]*x3*x3+\
                ra[4]*x0*x1+ra[5]*x0*x2+ra[6]*x0*x3+ra[7]*x1*x2+ra[8]*x1*x3+\
                ra[9]*x2*x3
        for vert in sc.vertices():
            f_val[tuple(vert)]=func_val(ra,vert)
        return f_val
    
    fv=np.zeros(sc.shape+[3])
    for i in range(3):
        fv[:,...,:,i]=func_rand_quadratic_scalar(sc,rounding)
    func_val_abs=np.zeros(sc.shape)
    func_sim=np.zeros(sc.shape, dtype='int')
    for vert in sc.vertices():
        f_val=fv[tuple(vert)]
        func_val_abs[tuple(vert)]=round(norm(f_val),rounding)
        func_sim[tuple(vert)]=simplicial_map(f_val)
    return func_val_abs,func_sim


def plot2d(array2d,tit):
    plt.clf()
    plt.imshow(array2d)
    plt.colorbar()
    plt.ion()
    plt.title(tit)
    plt.show()

def plot1d(array1d):
    plt.clf()
    plt.plot(array1d)
    plt.show()


if __name__=="__main__":
    tor=False
    sc=cubical_sc_torus([20,20,20,20])
    t=time()
    #sqrt_gamma=generate_gamma_polydecrease(sc.shape,3.0,tor)
    sqrt_gamma=generate_gamma_gaussian(sc.shape,0.2,tor)
    print("sqrt_gamma computed in:", time()-t)
    #print(sqrt_gamma)
    rand=Gaussian_Field_gamma(sc.shape,1,sqrt_gamma,tor)
    plot2d(rand[:,:,0,0,0])
    
