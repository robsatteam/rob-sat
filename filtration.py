from Cochain import *
from numpy import Inf
from cubical_sc import *
from cubical_sc_torus import *

def vertex_in_A(func_val_abs,vertex,r):
    return func_val_abs[tuple(vertex)]>=r
    
def cube_filtration(func_val_abs,sc,cube):
    return min([func_val_abs[tuple(v)] for v in sc.cube_vertices(cube)])
    
def simplex_filtration(func_val_abs,sim):
    twice_bary=sim.vertex_first()+sim.vertex_last()
    vertex,span=twice_bary//2, twice_bary%2
    args=[range(span[i]+1) for i in range(sim.dim_ambient)]
    prod=list(product(*args))
    supercube_vertices=[vertex+np.array(item) for item in prod]
    mm=min([func_val_abs[tuple(v)] for v in supercube_vertices])
    return mm

def simplex_filtration_old(func_val_abs,sc,sim):
    return cube_filtration(func_val_abs,sc,sc.smallest_supercube(sim))

def cube_in_A(func_val_abs,sc,cub,r):
    return cube_filtration(func_val_abs,sc,cub)>=r

def simplex_in_A(func_val_abs,sim,r):
# this is the cubical version: a simplex is in A iff its smallest supercube is in A
    return simplex_filtration(func_val_abs,sim)>=r

def cochain_maxr(func_val_abs,cochain):
    if cochain==Cochain({}):
        return -Inf
    return max([simplex_filtration(func_val_abs,item) for item in cochain])
        
def cochain_minr(func_val_abs,cochain):
    if cochain==Cochain({}):
        return Inf
    return min([simplex_filtration(func_val_abs,item) for item in cochain])

def cube_cochain_maxr(func_val_abs,cochain):
    if cochain==Cochain({}):
        return -Inf
    return max([cube_filtration(func_val_abs,sc,item) for item in cochain])
    
def k_cubes_r(sc,func_val_abs,k):
    if k<0:
        return dict()
    out=dict()
    for cub in sc.k_cube_list(k):
       out[cub]=min([func_val_abs[tuple(v)] for v in sc.cube_vertices(cub)])
    return out

def min_onbound(sc,func_val_abs):
    min_bound=Inf
    for v in sc.vertices():
        if sc.is_on_boundary(v) and func_val_abs[tuple(v)] < min_bound:
            min_bound=func_val_abs[tuple(v)]
    return min_bound
    
def max_filtration_sc(func_val_abs,cocycle): # max filtration value of simplicial cochain
    max_f=0.0
    for sim in cocycle:
        max_f=max(max_f, simplex_filtration(func_val_abs,sim))
    return max_f
    
    
if __name__=="__main__":
    sc=cubical_sc_torus([3,3])
    print(sc.vertices())
    func_val_abs