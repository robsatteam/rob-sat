def change_minus_ones_to_zeros(sequence):
    for i in range(len(sequence)):
        for j in range(len(sequence[i])):
            seq_new=[0 if x==-1 else x for x in sequence[i][j]]
            sequence[i][j]=seq_new
    return sequence

import sequences as sq
import numpy as np
from cubical_sc import *
from cubical_sc_torus import *
from simplex import *
from Cochain import *

def EML(cube,sequence_trimmed=None,trim = 0):
    #cube is Cube subclass of np.array with coordinates of the baricenter of the cube
    span = cube.span()
    vertex = cube.vertex()
    m = len(cube)
    k = sum(span)
    forbidden_span = np.array([-1]*m)+(span)
    sequences = change_minus_ones_to_zeros(sq.sequences(list(forbidden_span),k))
    if sequence_trimmed==None:
        sequences_all = sequences
    else:
        def clear_trimmed_sequence(sequence_trimmed,trim):
            result = [np.array(element) for element in sequence_trimmed]
            for i in range(len(sequence_trimmed)):
                for j in range(trim):
                    result[i][j] = 0
            return result                
            
        def shuffle(sequence,sequence_trimmed,trim):
            if len(sequence)==0 and len(sequence_trimmed)==1:
                return [[tuple(sequence_trimmed[0])]]
            if len(sequence)==1 and len(sequence_trimmed)==0:
                return [[tuple(sequence[0])]]
            result = []
            if len(sequence_trimmed)>0:
                result += shuffle(sequence,sequence_trimmed[:-1],trim)
                for seq in result:
                    seq.append(tuple(sequence_trimmed[-1]+seq[-1]))
            if len(sequence)>0:
                result2 = shuffle(sequence[:-1],sequence_trimmed,trim)
                for seq in result2:
                    app_elt = np.array(sequence[-1])
                    app_elt[trim:]+=seq[-1][trim:]
                    seq.append(tuple(app_elt))
                result += result2
            return result
        
        list_of_sequences = list()
        sequence_trimmed = clear_trimmed_sequence(sequence_trimmed,trim)
#        print "sequence_trimmed cleared:",sequence_trimmed
#        print "sequences obtained from vertex:", sequences
        for sequence in sequences:
#            print "Shuffles for a sequence ",sequence," are:"
            list_of_sequences.append(shuffle(sequence,sequence_trimmed,trim))
#            for sequence in list_of_sequences[-1]:
#                print sequence
        sequences_all = []
        for sequences in list_of_sequences:
            sequences_all.extend(sequences)
    result = Cochain()
    for sequence in sequences_all:
        simpl = simplex(vertex,sequence)
        result[simpl]=simpl.orientation()
    return result
    
#print EML(Cube([1, 1, 0, 0]), ((0, 0, 0, 1), (0, 0, 1, 0)), 2)


# AW prijima simplexy popsane pomoci "differencnich" posloupnosti: libovolne 
# dva prvky maji prazdny OR
def AW_diff(sc,vertex,sequence,iter = 0, iter_max = None):
    trivial = False #test of nontriviality of the simplex sequence[iter:]
    for element in sequence:
        if sum(element[iter:])==0:
            trivial = True
    if trivial:
        return set()
    if iter_max!=None and iter==iter_max:
        return{(sc.twice_baryc2cube(vertex),sequence,iter)}
    elif iter==len(vertex):
        if 0==len(sequence): 
            return {sc.twice_baryc2cube(np.array(vertex))}
        else: 
            print("I HSOULD NOT BE HERE") #we should not hit this branch du to 
            return set() #nontriviality of the simplex sequence[iter:]
    else:
        result = AW_diff(sc,vertex,sequence,iter+1,iter_max)
        if len(sequence)>0 and sequence[0][iter]==1:
            new_vertex = list(vertex)
            new_vertex[iter]+=1
            for i in range(iter+1,len(vertex)):
                new_vertex[i]+=2*sequence[0][i]
            #print "new vertex:",new_vertex
            result = result | AW_diff(sc,tuple(new_vertex), sequence[1:],iter+1,iter_max)
        return result
#print AW_diff(np.array([2,3,4]),[[1,0,0],[1,1,1]])

#print EML(np.array([1,1]))
def AW(sc,sequence,iter_max=None):
    if len(sequence) == 0: return set()
    dim = len(sequence[0])
    for element in sequence:
        if len(element)!=dim: 
            raise Exception("invalid input to AW: inconsistent lenghts")
    t_sequence = [None]*len(sequence)
    t_sequence[0]=tuple(sequence[0])
    for i in range(1,len(sequence)):
        t_sequence[i] = tuple(np.array(sequence[i])-np.array(sequence[i-1]))
    return AW_diff(sc,(0,)*dim,tuple(t_sequence),0,iter_max)
    
#print AW([[1,1,0],[1,1,1]])    
    
def SHI(sequence,trim):#currently I don't check for degeneracy of simplices
    i = 0
    while i<len(sequence) and sequence[i][trim]==0:
        i+=1
    if i==len(sequence):#projection onto the first coordinate sends it to only a vertex
        return []
    if not np.array(sequence[i][trim+1:]).any(): #if the simplex is of a "target" type
        return []
    sequence = list(sequence)
    element = list(sequence[i]) #zero-out the leading one of the i-th element
    element[trim] = 0    
    sequence[i] = tuple(element)
    #print "i-th element after clearing the leading one:", sequence[i]
    length = len(sequence[0])  #and transform the leading one into a separate element
    inserted = tuple([0]*trim+[1]+[0]*(length-trim-1))
    #print "the leading one transformed into a separate element", inserted 
    result = dict()
    for j in range(i+1,len(sequence)+1):#nasledujici by slo zefektivnit
        result_sequence = [tuple(element) for element in sequence]
        result_sequence.insert(j,tuple(inserted))
        result[tuple(result_sequence)] = (-1)**j #or j+1, we have to try and see
    return result
        
#result = AW(((1,1,0,0),(1,1,1,0),(1,1,1,1)),0)    
#print "AW_0:", result
#for (cube,sequence,trim) in result:
#    chain = SHI(sequence,trim)
#    print chain

#        SHI1 = Id \otimes SHI
#SHI + EML_1SHI_1AW_1 + EML2 SHI2 AW2 AW1 + 

if False:
    sequence_test = [[1,1,0,0],[1,1,1,0],[1,1,1,1]]    
    simpl = simplex(np.array([0,0,0,0]),sequence_test)
    #compute \partial SHI
    AW0 = AW(sequence_test,0)
    print("AW0:", AW0)
    partial_SHI = Cochain()
    for (cube,sequence,trim) in AW0: #for trim=0 is "for" useless,AW0 is a singleton
        SHI0 = SHI(sequence,trim)
        print("SHI0 (should appear just once):",SHI0)
        for sequence in SHI0:
            partial_SHI.add_cochain(Cochain(simplex(cube,sequence).boundary())*SHI0[sequence])
    print(partial_SHI)
            
    boundary = simpl.boundary()
    SHI_partial = Cochain()
    for (simpl1,coef) in boundary:
        AW0 = AW(simpl1.sequence_,0)
        for (cube,sequence,trim) in AW0:
            SHI0 = SHI(sequence,trim)
            for sequence in SHI0:
                SHI_partial.add_singleton(simplex(simpl1.vertex_,sequence),coef*SHI0[sequence])
#    print SHI_partial
    rh_side = Cochain(partial_SHI)
    rh_side.add_cochain(SHI_partial)
#    print "rh side:", rh_side
    
    lh_side = Cochain(((simpl,1),))
    AW1 = AW(sequence_test,1)
    
    for (cube,sequence,trim) in AW1:
        print("-EML for ",(cube,sequence,trim)," :",-EML(cube,sequence,trim))
        lh_side.add_cochain(-EML(cube,sequence,trim))
#    print "lh side:", lh_side


def AW_matrix(sc,dim,k):
    AW_matrix = dict()
    sequences = sq.sequences([0]*dim,k)
    for sequence in sequences:
        t_sequence = tuple([tuple(element) for element in sequence])
        #print t_sequence
        cubes = AW(sc,sequence)
        for cube in cubes:
            #print("cube:", cube)
            #print(sc.cube_twice_barycenter(cube))
            span = tuple(sc.cube_span(cube))
            vertex = sc.cube_vertex(cube)
            if not (span in AW_matrix):
                AW_matrix[span] = Cochain()
            AW_matrix[span].add_singleton(simplex(-vertex,sequence),1)  
    return AW_matrix           

if False:        
    chain = EML(Cube([1, 0, 1, 1, 1]))#, ((0, 0, 0, 0, 1), (0, 0, 0, 1, 0)), 3)
    print(chain)
    for simplex in chain:
        print(AW(simplex.sequence_))
elif False:
    dim = 4 #reprezentuje hodnotu m, dimenzi ambientniho \R^m
    k = 3 #reprezentuje libovolny stupen, kde poustime E-Z redukci, u nas primarne k=n
    
    AW_matrix = dict()
    
    sequences = sq.sequences([0]*dim,k)
    for sequence in sequences:
        t_sequence = tuple([tuple(element) for element in sequence])
        print(t_sequence)
        hybrids = AW(sequence,2)
        print(hybrids)
elif False:
    dim = 2 #reprezentuje hodnotu m, dimenzi ambientniho \R^m
    k = 1 #reprezentuje libovolny stupen, kde poustime E-Z redukci, u nas primarne k=n
    
    AW_matrix = dict()
    
    sequences = sq.sequences([0]*dim,k)
    for sequence in sequences:
        t_sequence = tuple([tuple(element) for element in sequence])
        #print t_sequence
        cubes = AW(sequence)
        for cube in cubes:
            span = cube.span()
            vertex = cube.vertex()
            if not (span in AW_matrix):
                AW_matrix[span] = Cochain()
            AW_matrix[span].add_singleton(simplex(-vertex,sequence),1)
            
    print(AW_matrix)

if __name__=="__main__":
    sc=cubical_sc([3,3,3])
    print(AW_matrix(sc,3,2))