### Rob-Sat ###

Rob-Sat is a software for testing the existence of a robust zero of a continuous vector-valued function f:  X → R^n. It computes lower and upper bounds on the robustness of zero of the function. The robustness of zero is a maximal number r such that each continuous g:  X → R^n, closer than r to f in the max-norm, has a zero. In other words, each continuous g with ||g-f|| <= r, has a zero.

This allows us to verify the existence of zeros of some g: X → R^n even in cases when the function g is not known precisely. E.g. when the function is obtained by an imprecise numerical process, or there is an uncertainty attached to some data defining the function. 

In the current implementation, X is assumed to be an m-cube [-1,1]^m or an m-torus (S^1)^m and the function to be given by values in a cubical grid and a Lipschitz constant. 
Details about the algorithm are in [this paper](https://arxiv.org/abs/1607.06344).


### Short manual ###

#### Input ####

The input to the program is a subdivision of the unit hypercube [-1,1]^m and function values in the vertices of a cubical grid. There are two ways how to call it: either by

> python rob-sat.py -f 'datafile.npy'

where 'datafile.npy' is a multidimensional numpy array of dimension m+1 where the evaluation on the first coordinate x_1,...,x_m represents the n-vector of function values.

An example file, for the 2 dimensional identity function, is in the repository ('data.npy').

Another possible input is

> python rob-sat.py -u g m 'formula'

where g is the grid size in each dimension, m is the dimension of the domain [-1,1]^m and 'formula' is a python expression representing a function f from [-1,1]^m to R^n. This expression is assumed to be a python list of its n components and the variables are denoted by X[0],X[1],... For example,

> python rob-sat.py -u 10 1 '[0.5-X[0]]'

represents a scalar function which has, in 10 grid values -1, -1+2/9, -1+4/9,..., 1, function values f(x)=0.5-x.

An example of a function with a 2-dimensional domain and range is the identity function

> python rob-sat.py -u 10 2 '[X[0],X[1]]'

#### Output ####

The program computes a number called "robustness of primary/secondary obstruction". The certified robustness of zero of f is r-α, where α is a simplexwise Lipschitz constant for f, a constant such that any x,y contained in one simplex satisfy |f(x)-f(y)| < α (wrt. the max-norm).

If n<3 or dim X <= n+1, then r+3α is an upper bound on robustness. 
The details are found in [the paper](http://arxiv.org/abs/1607.06344). 
It is up to the user to have an estimate of α, but an estimate of α for the case when f is assumed to be PL can be obtained with a parameter -a.

A positive lower bound r-α>0 is a certificate of the existence of zero of f.

For example, 

> python rob-sat.py -u 10 2 '[X[0],X[1]]' -a

returns the following output:

>primary obstruction dies at r=1.0, no higher obstructions

>if f is PL, then the simplexwise Lipschitz constant is: 0.222222

>and f has a robust zero

This means that the robustness of zero is at least 1.0-0.22... In other word, every continuous g such that ||g-f||<0.77..., has a zero. 

### Examples ###

#### Quadratic map of degree 2 ####

An approximation of the complex function f(z)=z^2 on a grid 20x20 can be called by

> python rob-sat.py -u 20 2 '[X[0]\*\*2-X[1]\*\*2,2\*X[0]\*X[1]]'

In the output, we see that the robustness of obstructions is R=0.86427. This means that if α is a simplexwise Lipschitz constant, then the robsutness of zero of the (unknown) function f is between R-α and R+3α. If f is simplexwise linear, then these numbers are computed to be 0.6537 and 1.4958. Note that increasing the grid size---in other words, increasing our knowledge about f---yields a more precise computation for the robustness of zero of the underlying function f(z)=z^2$. This can be seen by running

> python rob-sat.py -u 200 2 '[X[0]\*\*2-X[1]\*\*2,2\*X[0]\*X[1]]' -a.

In the output, the persistence of non-extendability is R=0.83417. However, the constant α is only 0.201, yielding a much more precise estimate of
the robustness of the underlying function. 

#### Quadratic map of degree 0 ####

It can be shown that the map f(x,y,z)=(x2 -y2 -z2, 2xy, 2xz) has a single zero in the origin that has index 0 and can be removed by arbitrary small perturbations. Using an approximation of this function we can run 

> python rob-sat.py -u 20 3 '[X[0]\*\*2-X[1]\*\*2-X[2]\*\*2,2\*X[0]\*X[1],2\*X[0]\*X[2]]'

The output shows that the obstruction to extendability dies immediately at R=0.00554 which indicates the non-robustness of zero. Unfortunately, the upper bound on robustness R+3α is quite large due to the relatively large simplexwise Lipschitz constant α (assumed the function is simplexwise linear): it could be improved by refining the grid. Obviously, the output finds no lower bound for the robustness.

#### Nontrivial secondary obstruction in the Hopf map ####

A more interesting example is a function from a 4-cube to R^3 that on the boundary maps the 3-sphere to a 2-sphere like the homotopically nontrivial Hopf map. This can be approximated by

> python rob-sat.py -u 10 4 '[2\*X[0]\*X[2]+2\*X[1]\*X[3],2\*X[1]\*X[2]-2\*X[0]\*X[3],X[0]\*\*2+X[1]\*\*2-X[2]\*\*2-X[3]\*\*2]'

The computed persistence of non-extendability R=0.79012 is close to the true robustness of zero; however, due to the course grid and large simplex-wise Lipschitz constant (assuming the function is simplex-wise linear), there is no guarantee that lower bound of robustness is positive. This could be achieved by using a larger grid gm=264 but then requirements on memory and time are already remarkable, see Section 5.1 of the paper for more experimental results.

### Further parameters ###

The argument -t indicates that we are on a torus, thus the last grid element in each dimension is adjucent to the first grid dimension. For instance, in a 2-dimensional grid of size 10x10, elements [3,0] and [3,9] are adjacent.

The norm is assumed to be ||f||:=max {|f(x)|} where |.| is a fixed norm in the Euclidean space. Implicitely, we consider the max-norm in R^n: it can be changed in the first lines of the file gen_fun.py. With parameter -r we may generate a random Gaussian fields. For instance,

> python rob-sat.py -r 10^2 2 3.0

creates a 2-dimensional grid 10x10, the range is 2-dimensional (two independent components are generated), and the last parameter 3.0 is a parameter of randomness.