from cubical_sc_torus import *
from cubical_sc import *
from Cochain import *
from hom_algebra import *
from time import *
from Steenrod import *
from generate_fun import *
#import matplotlib as plt
from marek_Z import persistence_of_cycle, persistent_cycles

import sys
import numpy as np
#import cProfile
import argparse


# This function is connected to the persistent generators
def pers(sc,vert,forb,n,r2,matrix_permuted,nm1_cubes,nm1_cubes_inv,nm1_sorted,nm1_filtration):
    def reindex_rows(col,row2ids):
        return [(row2ids[item[0]],item[1]) for item in col]

    if debug: 
        with open(dfile,'a') as df: df.write("   internal n:"+str(n)+'\n')

    # copypasted from the main body (unoptimal)
    #matrix_nm1, nm1_cubes,nm1_cubes_inv = sc.coboundary_matrix(vert,forb,n-1)
    #if debug: 
    #    with open(dfile,'a') as df:
    #        df.write('   Nrs of n-1:'+str(len(nm1_cubes))+'\n')
    #        df.write("   computing filtration..\n")
    #t=time()
    #nm1_filtration=[(cube_filtration(func_val_abs,sc,nm1_cubes[i]),i) for i in range(len(nm1_cubes))]
    #if debug: 
    #    with open(dfile,'a') as df:
    #        df.write("   done in "+str(time()-t)+'\n')
    #        df.write("   sorting..\n")
    #nm1_sorted=sorted(nm1_filtration)
    #matrix_permuted=[matrix_nm1[nm1_sorted[i][1]] for i in range(len(nm1_cubes))]
    # end copypast
    nm1_cube2order, nm1_order2cube={}, {} # creating the dictionary nm1-cube:its order in the filtration
    for j in range(len(nm1_sorted)): # for example, nm1_sorted=[(0.1,123), (0.2,12),...]
        nm1_cube2order[nm1_cubes[nm1_sorted[j][1]]]=j
        nm1_order2cube[j]=nm1_cubes[nm1_sorted[j][1]]
    matrix_nm2,nm2_cubes,nm2_cubes_inv = sc.coboundary_matrix(vert,forb,n-2)
    if debug: 
        with open(dfile,'a') as df:
            df.write('   Nrs of n-2 cubes:'+str(len(nm2_cubes))+'\n')
            df.write("   computing filtration..\n")
    if debug: t=time()
    nm2_filtration=[(cube_filtration(func_val_abs,sc,nm2_cubes[i]),i) for i in range(len(nm2_cubes))]
    #### TMP  ###############################################
    #nm2_filtration=[(0.01,i) for i in range(len(nm2_cubes))] ####
    ###  TMP  ###############################################
    if debug: 
        with open(dfile,'a') as df: df.write("   done in "+str(time()-t)+'\n')
    nm2_sorted=sorted(nm2_filtration)
    list2_ful=[item[0] for item in nm2_sorted]
    list1_ful=[item[0] for item in nm1_sorted]
    cut2=len([i for i in list2_ful if i<=r2])
    cut1=len([i for i in list1_ful if i<=r2])
    if debug: 
        with open(dfile,'a') as df: 
           df.write("   cut1: "+str(cut1)+"\n")
           df.write("   cut2: "+str(cut2)+"\n")
           df.write("   ordering columns and reindexing rows of the n-2 / n-1 matrix\n")
        t=time()
    matrix2_permuted=[reindex_rows(matrix_nm2[nm2_sorted[i][1]],nm1_cube2order) for i in range(len(nm2_cubes))]
    if debug: 
        with open(dfile,'a') as df: df.write("   done in "+str(time()-t)+'\n')
    matrix2=matrix2_permuted[0:cut2]
    matrix1=matrix_permuted[0:cut1]
    list1=list1_ful[0:cut1] 
    list2=list2_ful[0:cut2] 
    if debug: 
        with open(dfile,'a') as df: 
           df.write("   list1 last:"+str(list1[-1])+"\n")
           df.write("   list2 last:"+str(list2[-1])+"\n")
           df.write("   matrix1 nr of column: "+str(len(matrix1))+"\n")
           df.write("   matrix2 nr of column: "+str(len(matrix2))+"\n")
           df.write("   storing matrix12 and list12 to files...\n") 
        np.save('matrix1', matrix1) 
        np.save('matrix2', matrix2) 
        np.save('list1', list1) 
        np.save('list2', list2) 
        with open(dfile,'a') as df: df.write("   running persistent_cycles...\n") 
        t=time() 
    G,_=persistent_cycles(matrix1,matrix2,list1,list2)
    if debug: 
        with open(dfile,'a') as df: df.write("   done in "+str(time()-t)+" sec\n")
    r_values=[max([list1[item] for item in coc.keys()]) for coc in G]
    # need to transform is back
    return [cochain_conversion(cochain,nm1_order2cube) for cochain in G], r_values

# Main function: persistence of primary / secondary obstruction
def obstructions():
    if m<n: return "overdetermined system, no robust zero possible",-1.0
    if r0<0.0: return "no guarantee of zero", -1.0
    # PULLBACK:
    t=time()
    pull=pullback(sc, func_val_abs, func_sim, n, r0)
    #print(pull)
    if debug: 
        with open(dfile,'a') as df:
            df.write("pull length:"+str(len(pull))+'\n')
            df.write("pullback computed in"+str(time()-t)+'\n')
            df.write("pullback maximal filtration value: "+str(max_filtration_sc(func_val_abs,pull))+'\n')       
        assert check_cocycle(sc,pull,func_val_abs,r0,torus,0)
    if pull=={}: 
        if stats: 
            with open(sfile,'a') as sf: sf.write('-1.0\t no\t no')
        return "empty pullback", -1.0
    
    # PRIMARY OBSTRUCTION
    po_sim=codifferential_simplicial(sc,pull,torus)
    if debug: 
        with open(dfile,'a') as df:
            df.write("po_sim length:"+str(len(po_sim))+'\n')
            df.write("simplicial codifferential computed\n")
    po_cub=project_to_cubes(sc,po_sim)
    #if debug: 
    #    with open(dfile,'a') as df:
    #        df.write("projected to cubes\n")
    #        df.write("po_cub length"+str(len(po_cub))+'\n')
    #    assert check_cocycle_cubic(sc,po_cub,func_val_abs,n,0)
    #    assert check_cocycle(sc,po_sim,func_val_abs,n,torus,0)
    #    pull_cub=project_to_cubes(sc,pull)
    #    assert po_cub==codifferential_cubic(sc,pull_cub)
    if debug: 
        with open(dfile,'a') as df: df.write("creating the n-1 / n coboundary matrix\n")
    vert,forb=sc.zero_cubes()
    matrix_nm1, nm1_cubes,nm1_cubes_inv = sc.coboundary_matrix(vert,forb,n-1)
    if debug: 
        with open(dfile,'a') as df:
            df.write('Nrs of n-1:'+str(len(nm1_cubes))+'\n')
            df.write("computing filtration..\n")
        t=time()
    nm1_filtration=[(cube_filtration(func_val_abs,sc,nm1_cubes[i]),i) for i in range(len(nm1_cubes))]
    if debug: 
        with open(dfile,'a') as df: 
            df.write("  done in "+str(time()-t)+'\n')
            df.write("sorting..\n")
    nm1_sorted=sorted(nm1_filtration)
    matrix_permuted=[matrix_nm1[nm1_sorted[i][1]] for i in range(len(nm1_cubes))]
    #if debug: df.write("matrix_permuted:", matrix_permuted)
    if debug: 
        with open(dfile,'a') as df:
            df.write("running Hubert\n")
        matrix_permuted_backup=deepcopy(matrix_permuted)
        t=time()
    hub,last=persistence_of_cycle(matrix_permuted,list(po_cub.items()),0)
    if debug: 
        with open(dfile,'a') as df: df.write("  done in time:"+str(time()-t)+'\n')
    if last<0: r1=0.0 # last = -1 means that the solution is trivial: it can only happen if the rhs is empty
    else: r1=nm1_sorted[last][0]
    if debug: 
        with open(dfile,'a') as df: df.write("r where primary dies:"+str(r1)+'\n')
    if stats: 
        with open(sfile,'a') as sf: sf.write(str(r1)+'\t')
        if r1>r0: 
            with open(sfile,'a') as sf: sf.write('yes\t') 
        else: 
            with open(sfile,'a') as sf: sf.write('no\t')
    hub_cub={}
    for item in hub:
        hub_cub[nm1_cubes[nm1_sorted[item][1]]]=hub[item]
    # hub_cub is the cubical cochain s.t. delta hub_cub = -po_cub
    #if debug:
    #    M_x=matrix_times_vector(matrix_permuted_backup,hub)
    #    assert(cochain_addition(dict(M_x), po_cub)==Cochain())
    #    #pull_cub=project_to_cubes(sc,pull)
    #    assert(cochain_addition(codifferential_cubic(sc,hub_cub), po_cub)==Cochain())
    if n<3 or m==n:
        if r1>r0: return "primary obstruction dies at r="+str(r1)+", no higher obstructions",r1
        else: return "no obstructions at r0="+str(r0)+" level", -1.0
    else:
        if r1>r0:
            prim_res="primary obstruction dies at r="+str(r1)
            if debug: 
                with open(dfile,'a') as df: df.write(prim_res)
            if primary: return prim_res, r1
        else:
            prim_res="no primary obstructions at r0="+str(r0)+" level"
            if debug: 
                with open(dfile,'a') as df: df.write(prim_res)
            if primary: return prim_res, -1.0
    # End of primary obstruction part
    
    # COMPUTING EXTENSION (NEEDED FOR SECONDARY OBSTRUCTION)
    if debug: 
        with open(dfile,'a') as df: df.write("computing AW_hub\n")
    AW_hub=project_to_simplices(sc,hub_cub,n-1,torus)
    #if debug:
    #    E_AW_hub=project_to_cubes(sc,AW_hub)
    #    with open(dfile,'a') as df: 
    #        df.write("len E_AW_hub:"+str(len(E_AW_hub))+'\n')
    #        df.write("length of AW_hub:"+str(len(AW_hub))+'\n')
    #    assert cochain_subtraction(hub_cub,E_AW_hub)==Cochain({}), "E_AW hub is not hub (?)"
    #    with open(dfile,'a') as df: df.write("computing delta_AW_hub...\n")
    delta_AW_hub=codifferential_simplicial(sc,AW_hub,torus)
    #if debug:
    #    with open(dfile,'a') as df: 
    #        df.write("length of delta_AW_hub:"+str(len(delta_AW_hub))+'\n')
    #        df.write("computing E_delta_AW_hub...\n")
    #    E_delta_AW_hub=project_to_cubes(sc, delta_AW_hub)
    #    assert cochain_addition(E_delta_AW_hub,po_cub)==Cochain({}), "E delta AW is not equal to -fo_cub (?)"
    #    delta_hub=codifferential_cubic(sc,hub_cub)
    #    AW_delta_hub=project_to_simplices(sc,delta_hub,n,torus)
    #    assert AW_delta_hub==delta_AW_hub
    
        # Theory:
        #delta AW h = AW delta h = - AW fo_cub = - AW E fo = - fo - (delta SHI + SHI delta) fo
        #delta AW h = - fo - delta SHI fo
        #delta (AW h + SHI fo)=-fo
        #delta (SHI fo)=-(fo+delta AW h)
        #let corr:=SHI fo 
        # this is computed by 
        # Hubert(matrix of codifferentials on suitable n-1-simplices, fo+delta AW h)
        #then delta(AW h + corr)=-fo
        #delta (pull + AW h + corr)=0
        #the desired Extension equals 
        #pull + AW h + corr
    
    if debug: 
        with open(dfile,'a') as df: df.write("creating the SHI linear system..\n")
    t=time()
    rhs=cochain_addition(delta_AW_hub,po_sim)
    if debug:  
        #assert project_to_cubes(sc,rhs)=={}, "E(fo_sim+delta_AW_hub) is not zero (?)"
        with open(dfile,'a') as df: df.write("len(rhs):"+str(len(rhs))+'\n')
    matrix_SHI,vector_SHI,nm1_filtration2sim = SHI_linear_problem(sc,func_val_abs,po_sim,rhs,n)
    if debug: 
        with open(dfile,'a') as df: df.write("  done in "+str(time()-t)+"sec\n")
        matrix_backup_SHI=deepcopy(matrix_SHI)
        vector_backup_SHI=copy(vector_SHI)
    if debug: 
        with open(dfile,'a') as df: df.write("solving SHI...\n")
    if vector_SHI=={} and debug: df.write("!!!!!!!!!!!!\n")
    correction_ids, last = persistence_of_cycle(matrix_SHI, vector_SHI, 0)
    corr=cochain_conversion(correction_ids,nm1_filtration2sim)     
    if debug: 
        #assert cochain_addition(Cochain(matrix_times_vector(matrix_backup_SHI,correction_ids)), vector_backup_SHI)==Cochain()
        with open(dfile,'a') as df: df.write("minr pull:"+str(cochain_minr(func_val_abs,pull))+'\n')
        maxr_AW_hub=cochain_maxr(func_val_abs,AW_hub)
        with open(dfile,'a') as df: df.write("maxr_AW_hub:"+str(maxr_AW_hub)+'\n')
        maxr_corr=cochain_maxr(func_val_abs,corr)
        with open(dfile,'a') as df: 
            df.write("maxr_corr:"+str(maxr_corr)+'\n')
            df.write("computing extension..\n")
    extension=cochain_addition(pull,AW_hub)
    extension=cochain_addition(extension,corr)
    #if debug:
    #    assert check_cocycle(sc,extension,func_val_abs,0,torus,0)
    #    with open(dfile,'a') as df: 
    #        df.write("extension extends pull for r >"+str(max(r1,r0))+'\n')
    #        df.write("len extension:"+str(len(extension))+'\n')
    #    diff=cochain_subtraction(extension,pull)
    #    assert cochain_zero_in_A(func_val_abs,diff,max(r0,r1)+eps), "extension != pull for r>r1"    
    if debug:
        with open(dfile,'a') as df: 
            df.write("extension maximal filtration value: "+str(max_filtration_sc(func_val_abs,extension))+'\n')       

    # SECONDARY OBSTRUCTION
    if debug: 
        with open(dfile,'a') as df: df.write("computing Steenrod square...\n")
        t=time()
    Steen_ext=steenrod_square(n,sc,extension,func_val_abs,func_sim,torus)
    #if debug and n==3:
    #    Steen_cup=cup_square_dim2(sc,extension,extension,func_val_abs,func_sim,torus)
    #    assert Steen_cup==Steen_ext
    #    with open(dfile,'a') as df: df.write("len Steen:"+str(len(Steen_cup))+'\n')
    #if debug:
    #    with open(dfile,'a') as df: df.write("  done in "+str(time()-t)+'sec\n')
    #    assert(cochain_zero_in_A(func_val_abs,Steen_ext,max(r0,r1)+eps)), "Steenrod nonzero on A_{r1+}"
    #    assert(codifferential_simplicial(sc,Steen_ext,torus)==Cochain()), "Steenrod is not a cocycle"
    #    with open(dfile,'a') as df: df.write("projecting Steenrod to cubes..\n")
    Steen_ext_cub=project_to_cubes(sc,Steen_ext)
    matrix_n, n_cubes,n_cubes_inv = sc.coboundary_matrix(vert,forb,n)
    if debug: 
        with open(dfile,'a') as df: 
            df.write('Nrs of n cubes:'+str(len(n_cubes))+'\n')
            df.write("computing filtration..\n")
        t=time()
    n_filtration=[(cube_filtration(func_val_abs,sc,n_cubes[i]),i) for i in range(len(n_cubes))]
    if debug: 
        with open(dfile,'a') as df: 
            df.write("  done in "+str(time()-t)+'\n')
            df.write("sorting..\n")
    n_sorted=sorted(n_filtration)
    matrix_n_permuted=[matrix_n[n_sorted[i][1]] for i in range(len(n_cubes))]
    matrix2_backup=deepcopy(matrix_n_permuted)
    if n==3: p=0
    else: p=2
    Steen_ext_cub=Cochain(dict(Steen_ext_cub)).mod_p(p) # get rid of zeros, reduce mod p
    rhs_backup=copy(Steen_ext_cub)
    if debug and p==2: 
        with open(dfile,'a') as df: df.write("removed zeros in Steen_ext_cub...\n")
    if debug: 
        with open(dfile,'a') as df: df.write("running Hubert\n")
    try:
        t=time() 
        hub2,last2=persistence_of_cycle(matrix_n_permuted,list(Steen_ext_cub.items()),p)
        if debug: 
            with open(dfile,'a') as df: df.write("time:"+str(time()-t)+'\n')
        if last2<0: r2=0.0
        else: r2=n_sorted[last2][0]
    except:
        if debug: 
            with open(dfile,'a') as df: 
                df.write("exception: probably no solution found\n")
                df.write('St_ext is a nontrivial global cohomology\n')
        if outputfile: 
            with open(ofile,'a') as of: of.write('St_ext is a nontrivial global cohomology\n')
        r2=np.Inf
        #assert False # breaking, something is wrong
    
    if debug and r2<np.Inf:
        hub2_cub={}
        for item in hub2:
            hub2_cub[n_cubes[n_sorted[item][1]]]=hub2[item]
        if debug: 
            with open(dfile,'a') as df: df.write("checks of correctness...\n")
        c1=Cochain(matrix_times_vector(matrix2_backup, hub2))
        c2=Cochain(rhs_backup)
        assert (cochain_addition(c1,c2)).mod_p(p)==Cochain({}), "something is wrong the solution of the sec. obstr. persistence system"
        assert (cochain_addition(codifferential_cubic(sc,hub2_cub), Steen_ext_cub).mod_p(p)==Cochain()), "delta hub2 neq Sten_ext_cub"
    if debug: 
        with open(dfile,'a') as df: df.write("secondary obstruction dies no later than r="+str(r2)+'\n')
    if r2>max(r0,r1):
        if debug: 
            with open(dfile,'a') as df: df.write("possible nontrivial secondary obstruction detected\n")
        if outputfile: 
            with open(ofile,'a') as of: of.write('nontrivial St(ext) (r2='+str(r2)+')\n')
        if stats: 
            with open(sfile,'a') as sf: sf.write('possibly (ii='+str(ii)+', r2='+str(r2)+')')
        if n==3:
        #if False:
            if m==4 and not torus:
                return "obstructions die at r="+str(r2)+" (nontrivial secondary obstruction)", r2
            else: # here could be, if needed, more experiments for the special case  m==4 and torus 
            ###### QUADRATIC EQUATIONS..!! (Experimental) ########
                r2_max=r2
                r2=max(r0,r1) # +epsilon?
                if debug: 
                    with open(dfile,'a') as df: 
                        df.write("secondary obstruction dies no later than at"+str(r2)+'\n')
                        df.write("more tests are needed (persistent generators)\n")
                        df.write("storing into r2 the value r2="+str(r2)+"\n")
                matrix_permuted=[matrix_nm1[nm1_sorted[i][1]] for i in range(len(nm1_cubes))] # probably recomputation of something that had been computed before -- I don't think it is necesary
                G,r_values = pers(sc,vert,forb,n,r2) # seems to work (computes cohomology generators of $H^{n-1}(X,A_r)$ for 0<=r<=r2)
                if debug: 
                    with open(dfile,'a') as df: 
                        df.write("nr of G:"+str(len(G))+'\n')
                        df.write("Nr of r_values:"+str(len(r_values))+'\n')
                        df.write(str(r_values)+'\n')
                if G==[]:
                    return "obstructions die at r="+str(r2), r2
                matrix, t_temp,j=[], -1.0, 0
                r_temp=-1.0
                while r_temp<=r2:
                    matrix.append(matrix_n_permuted[j])
                    r_temp=n_sorted[j][0]
                    j+=1
                # matrix should contain the column of n/n+1 coboundary matrix corresponding to r<=r2
                if debug:
                    with open(dfile,'a') as df:
                        df.write("matrix created containing the columns of n/n+1 coboundary matrix corresponding to r<=r2 \n")
                        df.write("first column corresponds to r="+str(n_sorted[0][0])+"\n")
                        df.write("last column has id "+str(j-1)+" and corresponds to r="+str(n_sorted[j-1][0])+"\n")
                # steen squared (cubic) is in hub2
                matrix_r2=[list(x.items()) for x in matrix]
                matrix_r2_backup=deepcopy(matrix_r2)
                steen_reduced=persistence_of_cycle(matrix_r2, rhs_backup, p=0, change_basis=True, total_RHS_reduction = True)
                if debug:
                    with open(dfile,'a') as df:
                        df.write("Steen reduced:\n")
                        df.write(str(steen_reduced)+"\n")

                for i in range(len(G)):
                    if debug: 
                        assert codifferential_cubic(sc,G[i])==Cochain()
                        assert cochain_zero_in_A_cubic(sc,func_val_abs,G[i],r_values[i]+eps)
                    st_i=cup_prod_dim2_cubical(sc,rhs_backup,G[i])
                    if debug:
                        with open(dfile,'a') as df:
                            df.write("i="+str(i)+"\n")
                            df.write("  computed cup square of Steenrod and G"+str(i)+"\n")
                            df.write("  length:"+str(len(st_i))+"\n")
                    matrix_r2=deepcopy(matrix_r2_backup)
                    st_i_reduced=persistence_of_cycle(matrix_r2, st_i, p=0, change_basis=True, total_RHS_reduction = True)
                    if debug:
                        with open(dfile,'a') as df:
                            df.write("st times G"+str(i)+" reduced:\n")
                            df.write(str(st_i_reduced)+"\n")
                for i in range(len(G)):
                    for j in range(i,len(G)):
                        st_i_j=cup_prod_dim2_cubical(sc,G[i],G[j])
                        with open(dfile,'a') as df:
                            df.write("i, j="+str(i)+", "+str(j)+"\n")
                            df.write("  computed cup square of G"+str(i)+" and G"+str(j)+"\n")
                            df.write("  length:"+str(len(st_i_j))+"\n")
                        matrix_r2=deepcopy(matrix_r2_backup)
                        st_i_j_reduced=persistence_of_cycle(matrix_r2, st_i_j, p=0, change_basis=True, total_RHS_reduction = True)
                        if debug:
                            with open(dfile,'a') as df:
                                df.write("G"+str(i)+" times G"+str(j)+" reduced:\n")
                                df.write(str(st_i_j_reduced)+"\n")
                return None, None
            ###### END OF EXPERIMENTS WITH QUADRATIC EQUATIONS

        else: # n>3 

    # PERSISTENT GENERATORS (Fuck)
            if debug: 
                with open(dfile,'a') as df: 
                    df.write("secondary obstruction dies no later than at"+str(r2)+'\n')
                    df.write("more tests are needed (persistent generators)\n")
            r2=0.7
            if debug: 
                with open(dfile,'a') as df: 
                    df.write("r2 is set up to "+str(r2)+" (just for this case)\n")
            matrix_permuted=deepcopy(matrix_permuted_backup)
            G,r_values = pers(sc,vert,forb,n,r2,matrix_permuted,nm1_cubes,nm1_cubes_inv,nm1_sorted,nm1_filtration) 
						# seems to work (computes cohomology generators of $H^{n-1}(X,A_r)$ for 0<=r<=r2)
            if debug: 
                with open(dfile,'a') as df: 
                    df.write("nr of G:"+str(len(G))+'\n')
                    df.write("Nr of r_values:"+str(len(r_values))+'\n')
            if G==[]:
                if m==n+1:
                    return "obstructions die at r="+str(r2), r2
                else:
                    return "secondary obstruction dies at r="+str(r2), r2
            steenrods=[]
            for i in range(len(G)):
                #if debug: df.write("len G[", i, "]:", len(G[i]))
                if debug: 
                    assert codifferential_cubic(sc,G[i])==Cochain()
                    assert cochain_zero_in_A_cubic(sc,func_val_abs,G[i],r_values[i]+eps)
                st=steenrod_square_cubical(sc,G[i])
                if st!=Cochain():
                    steenrods.append((st,r_values[i]))
                    if debug: 
                        with open(dfile,'a') as df: 
                            df.write("steenrod nontrivial as cochain\n")
                            df.write("length of steenrod:"+str(len(st))+'\n')
                            df.write("r_val:"+str(r_values[i])+'\n')
            if steenrods==[Cochain() for i in range(len(steenrods))]:
                if debug: 
                    with open(dfile,'a') as df: df.write("empty steenrods\n")
                if m==n+1:
                    return "obstructions die at r="+str(r2), r2
                else:
                    return "secondary obstruction dies at r="+str(r2), r2
            else:
                ########### THIS HAS YET TO BE TESTED ON SOME EXAMPLE ... ##########!!!
                if debug: 
                    with open(dfile,'a') as df: 
                        df.write("nontrivial steenrods of generators, as cochains\n")
                        df.write("number of steenrods:"+str(len(steenrods))+'\n')
                        df.write("constructing mixed Steen / (n+1/n)-coboundary matrix\n")
                matrix_mixed, r_mixed, r,ind_cob, ind_steen, cont=[], [], 0.0, 0, 0, True
                while r <= r2 and cont:
                    if (ind_steen < len(steenrods)) and \
                       ((ind_cob >= len(n_sorted) or  \
                        n_sorted[ind_cob][0] > steenrods[ind_steen][1])):
                            # steenrods can be added AND
                            # (coboundaries can't be added OR it's steenrods turn)
                        matrix_mixed.append(steenrods[ind_steen][0].items())
                        r=steenrods[ind_steen][1]
                        r_mixed.append(r)
                        if debug: 
                            with open(dfile,'a') as df: 
                                df.write("appending steenrods..\n")
                                df.write("r_mixed appending:"+str(r)+'\n')
                        ind_steen+=1
                    elif (ind_cob < len(n_sorted)) and \
                        (ind_steen >= len(steenrods) or \
                        n_sorted[ind_cob][0] <= steenrods[ind_steen][1]):
                            # coboundary can be added AND
                            # (steenrods can't be added or it's coboundarys' turn)
                        matrix_mixed.append(matrix2_backup[ind_cob]) # adding a coboundary
                        r=n_sorted[ind_cob][0]
                        r_mixed.append(r)
                        ind_cob+=1
                    else:
                        cont = False # don't continue the loop
                # Final persistence computation:
                if debug:  
                    with open(dfile,'a') as df: df.write("final persistence computation over Z_2:\n") 
                try: 
                    hub3,last3=persistence_of_cycle(matrix_mixed,rhs_backup,2) # here we already need to compute over Z_2
                except:
                    if debug: 
                        with open(dfile,'a') as df: df.write("something is wrong: hub3 was not computed\n")
                    if outputfile: 
                        with open(ofile,'a') as of: of.write('something is wrong with hub3 computation\n')
                    assert False
                r3=r_mixed[last3]
                if r3<r2:
                    if debug: 
                        with open(dfile,'a') as df: df.write("Uao, r3<r2. Persistent generators were really useful :-)\n") # this will possibly never happen
                if m==n+1:
                    return "obstructions die at r="+str(r3), r3
                else:
                    return "secondary obstruction dies at r="+str(r3), r3
                ########### END OF "IT HAS YET TO BE TESTED" PART ... ##########!!!
    else: # r2 smaller than max(r0, r1)
        if stats: 
            with open(sfile,'a') as sf: sf.write('no')
        if debug: 
            with open(dfile,'a') as df: df.write("no secondary obstruction\n")
        if n==3:
            if r1>r0: return "primary dies at "+str(r1)+", no higher obstructions", r1
            else: return "no obstructions at r0="+str(r0), -1.0
        else:
            if r1>r0: return "primary dies at "+str(r1)+", no secondary obstructions", r1
            else: return "no primary/secondary obstruction at r0="+str(r0), -1.0


######## MAIN BODY STARTS HERE ############
# INPUTS:
parser = argparse.ArgumentParser(description="Approximating robustness of zero from below. \n \
    One of the options -f, -u, -r must be specified. \n")
group = parser.add_mutually_exclusive_group()
group.add_argument("-f", "--file", help="Reads the function values in vertices from FILE")
group.add_argument("-u", "--formula", nargs='+', help="computes the function values in vertices from the FORMULA")
group.add_argument("-r", "--random", nargs="+", help="generates random function")
parser.add_argument('-d', '--debug', action="store_true", help="debugging mode, log written to 'debugfile'")
parser.add_argument('-o', '--outputfile', action="store_true", help="writing important results into the file 'outputs'")
parser.add_argument('-t', '--torus', action="store_true", help="the domain is a torus (default: a box)", default=False)
parser.add_argument('-a', '--alpha', action="store_true", help="compute the simplexwise Lipschitz constant assuming the function is PL")
parser.add_argument('-p', '--primary', action="store_true", help="primary obstruction only")
parser.add_argument('-e', '--repetition', help="repeat computation REPETITION number of times", default="1")
args = parser.parse_args()
stats=False

#if False:
if True: # used for experiments
    #args.random="15^4", "3", "5.0"
    args.torus=True
    args.debug=True
    #args.formula=["10", "5", "[2*X[0]*X[2]+2*X[1]*X[3],2*X[1]*X[2]-2*X[0]*X[3],X[0]**2+X[1]**2-X[2]**2-X[3]**2, X[4]]"]
    #stats=True
    #args.repetition=1
    #args.outputfile=True
    args.file="data_torus54_poly_5.5_centered_nontrivial.npy"

if args.file==None and args.formula==None and args.random==None:
    print(parser.print_help())
    sys.exit()
torus=args.torus
debug=args.debug
outputfile=args.outputfile
computealpha=args.alpha
primary=args.primary
random=(args.random!=None)
repeat=int(args.repetition)

prime=0
eps=0.00000001 # small number, to replace >x by >=x+eps (used in asserts only) 
rounding=5
time_init=time()
if debug:
    dfile='debugfile'
    with open(dfile,'a') as df: df.write(ctime()+'\n')
if outputfile:
    ofile='outputs'
if stats:
    sfile='stats'

# Defining the simplicial/cubical complex
if args.file!=None:
    # Input function from file
    from_file=True
    if debug:
        with open(dfile,'a') as df: df.write("storing function values\n")
    inputfile=args.file
    func_arr=np.load(inputfile) # function values as an array
    sh=func_arr.shape
    n=sh[-1]
    m=len(sh)-1
    if torus: sc=cubical_sc_torus(sh[0:-1])
    else: sc=cubical_sc(sh[0:-1]) 
    if debug: 
        with open(dfile,'a') as df: df.write("input file: '"+inputfile+"'\n")
    
if args.formula!=None:
    # Input function from formula
    assert(len(args.formula)==3), "3 arguments needed in the -f option"
    from_file=False
    grid = int(args.formula[0])
    m=int(args.formula[1])
    formula=args.formula[2]
    def input_function(X):
        return eval(formula)
    def func(X): # transformation of the grid values to the [-1,1]^m box
        X=[-1.0+2.0*X[i]/(grid-1.0) for i in range(m)]
        return input_function(X)
    n=len(func([0]*m)) # dimension of the codomain
    if torus: sc=cubical_sc_torus([grid]*m)
    else: sc=cubical_sc([grid]*m)
    if debug: df.write("storing function values\n")
    func_arr=np.zeros(sc.shape+[n])
    for coords in product(*[range(sc.shape[i]) for i in range(sc.m)]):
        func_arr[coords]=func(coords)
if args.random!=None:
    # Function generated internally
    assert(len(args.random)>=3), "3 arguments needed in the -r option"
    sg,sm=re.split('\^',args.random[0])
    grid,m=int(sg),int(sm)
    n=int(args.random[1])
    rand_par=float(args.random[2])
    if torus: sc=cubical_sc_torus([grid]*m)
    else: sc=cubical_sc([grid]*m)
    if debug: 
        with open(dfile,'a') as df: df.write("computing sqrt_gamma\n")
    sqrt_gamma=generate_gamma_polydecrease(sc.shape,rand_par,torus) # no need to compute it repeatidly in the loop
if debug: 
    with open(dfile,'a') as df:
      df.write('shape of the simplicial complex:'+str(sc.shape)+'\n')
      df.write('m='+str(m)+' , n='+str(n)+'\n')

for ii in range(repeat): # repetition migh be used if random functions are generated

    # Specifying the simplicial and cubical complex and generating function values
    # 'sc' is the complex, 'func_arr' the multiarray of function values
    if outputfile:
        with open(ofile,'a') as of:
          of.write(ctime()+'\n')
          of.write('New fun\n')
          of.write('shape='+str(sc.shape)+'\n')
          of.write('m,n='+str(m)+','+str(n)+'\n')
          if repeat>1: of.write('attemt:'+str(ii)+'\n')
    if debug:
        with open(dfile,'a') as df:
          df.write('\n'+ctime()+'\n')
          df.write('New fun\n')
          df.write('ii='+str(ii)+'\n')
    if random:
        if debug: 
          with open(dfile,'a') as df: df.write("generating new random field\n")
        func_arr=Gaussian_Field_gamma(sc.shape,n,sqrt_gamma,torus,"centered")
    #plot2d(func_arr[:,:,10,10,10,0])
    #np.save('data'+str(ii),func_arr)
    if debug: 
      with open(dfile,'a') as df: df.write("computing filtration and simplicial approximation\n")    
    func_val_abs,func_sim= func_values(sc,func_arr,n,rounding,torus)
    #plot2d(func_val_abs)
    if debug:
      with open(dfile,'a') as df: df.write("computing minimal simplicial r..\n")
      t=time()
    r0=min_simplic_r(sc,func_val_abs,func_sim) # f' is simplicial if r is STRICTLY GREATER than the computed r0
    # temporary:
    print("r0:", r0)
    if debug:
      with open(dfile,'a') as df:
          df.write("  done in "+str(time()-t)+'\n')
          df.write("r0>"+str(r0)+'\n')
    if outputfile: 
        with open(ofile,'a') as of: of.write('r0:'+str(r0)+'\n')
    if stats: 
        with open(sfile,'a') as sf: sf.write(str(r0)+'\t')
    r0=r0+eps
    #if debug:
    #  t=time()
    #  assert check_simplicial(sc,func_val_abs,func_sim,r0+eps)
    #  with open(dfile,'a') as df: df.write("simpliciality checked in "+str(time()-t)+ "sec\n")

    if random and debug: 
        with open(dfile,'a') as df: df.write("calling the main function: obstruction(...), ii="+str(ii)+'\n')


    # Call of the main function (obstruction)
    ###########################################


    res,lb_obstr=obstructions() # outputs message and the lower bound for persistence of obstructions


    ###########################################


# PRINTING THE FINDINGS
    print('*************************')
    print(res)
    if outputfile:
      with open(ofile,'a') as of: of.write(res+'\n\n')
    if debug: 
        with open(dfile,'a') as df: df.write(res+'\n')
    if computealpha:
      alpha=approximate_alpha(sc,func_arr)
      print("if f is PL, then the simplexwise Lipschitz constant is: "+str(alpha))
      if lb_obstr>alpha:
          print("and f has a robust zero")
      else:
          print("no guarantee of zero")
    else:
      if lb_obstr >r0:
          print("robustness of zero is at least "+str(lb_obstr)+" minus the simplex-wise Lipchitz constant")
      else:
          print("no guarantee of zero")
    print('*************************')
    if stats:
        with open(sfile,'a') as sf: sf.write('\n')
    # End of the main for cycle
if debug: 
    with open(dfile,'a') as df: df.write("total time:"+str(time()-time_init)+'\n')
