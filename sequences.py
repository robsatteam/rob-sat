from copy import copy,deepcopy
from numpy import Inf, count_nonzero

def permutation_sign(perm):
    s=1
    l=len(perm)
    for i in range(l):
        for j in range(i,l):
            if perm[i]>perm[j]:
                s=-s
    return s

def find_first_zero(l): # finds the first zero in a list
    for i in range(len(l)):
        if l[i]==0:
            return i
    return None

def zero_to_one(l,m): # converts the mth element of a list l to 1
    return [l[i] if i!=m else 1 for i in range(len(l))]

def change_to_zeros(ll,m): 
# it creates and returns a new list (the input ll is preserved)
# in the new list, all elements l of the copy of ll it changes l[m] to 0
    out=deepcopy(ll)
    #out=copy(ll)
    for i in range(len(out)):
        out[i][m]=0
    return out

def subsets(l,k):
# creates a list of all vectors derived from l by changing j<=k zeros to ones
    noz=l.count(0)
    first_zero=find_first_zero(l)
    output=[]
    output.append(l)
    if noz==0 or k==0:
         return output # only the empty set
    l1=zero_to_one(l,first_zero) # first zero is changed to 1
    sub1=subsets(l1,k-1)  # all subsets of size at most k-1
    sub2=subsets(l1,k)    # all subsets of size k with 1 instead of 0 at position first_zero
    sub2=change_to_zeros(sub2, first_zero) # changes 1 back to 0
    output=sub1+sub2 # all subsets of size k: either they have 0 or 1 on position first_zero
    return output    

def subsets_nonempty(l,k): 
# creates a list of all vectors derived from l by changing 1<=j<=k zeros to ones
    output=subsets(l,k)
    output.pop()
    return output

def sequences(l,k):
# l is a list of zeros or nonzeros, k is an integer
# it produces the list of all increasing sequences of length k (nonzero elements are preserved)
    def join_sequences(subs, seq):
    # example: join_sequences([1,0,0], [[[1,0,1],[1,1,1]], [[1,1,0], [1,1,1]]])
    # outputs [[[1,0,0],[1,0,1],[1,1,1]],[[1,0,0],[1,1,0],[1,1,1]]]
        out=list()
        if len(seq)==0:
            return [[subs]]
        for i in range(len(seq)):
            out.append([subs]+seq[i])
        return out

    output=list()
    noz=l.count(0)
    if k==0 or noz<k:
        return []
    else:
        l1=subsets_nonempty(l,noz-k+1) # list of subsets of length at most noz-k+1
        for subs in l1:
            rec=sequences(subs,k-1)
            js=join_sequences(subs, rec)
            output=output+js
        return output

def graph_of_sequences(m,k): # returns a list of k dictionaries {subset:list of subsets}
    if k<2:
        return []
    dictionaries=[{} for i in range(k-1)]
    init_subsets=subsets_nonempty([0]*m,m-k+1)
    depth=0
    for s in init_subsets:
        dictionaries[0][tuple(s)]=subsets_nonempty(s,m-k+2+depth-count_nonzero(s))
    for depth in range(1,k-1):
        for subs in dictionaries[depth-1]:
            for s in dictionaries[depth-1][subs]:
                if tuple(s) not in dictionaries[depth]:
                    dictionaries[depth][tuple(s)]=subsets_nonempty(s,m-k+2+depth-count_nonzero(s))
    return dictionaries

#print sum([i for i in range(10)])
#print sys.getsizeof(graph_of_sequences(10,4))

def dict_of_sequences(size,k): # generates a dictionary
############# SHOULD BE REMOVED #######################
# first arg of the dictionary: subsets of 0,...,0 represented by vectors of 0's and -1's
# -1's indicate forbidden directions
# to each such subset the dictionary assigns 
# all growing 01-sequences of k elements that have zeros in the forbidden directions

    def change_minus_ones_to_zeros(lll):
    # it each item l of the list ll of the list lll, it changes -1 to 0
        for i in range(len(lll)):
            for j in range(len(lll[i])):
                seq_new=[0 if x==-1 else x for x in lll[i][j]]
                lll[i][j]=seq_new
        return lll

    subs_dic=dict()
    sub=subsets([0]*size,Inf)
    for s in sub:
        s=[-1 if x==1 else 0 for x in s] # converts 1 to -1
        subs_dic[tuple(s)]=change_minus_ones_to_zeros(sequences(s,k))
    return subs_dic
    
if __name__=="__main__":
    g=graph_of_sequences(3,2)
    print(g)
    #print d    