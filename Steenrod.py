# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 15:17:43 2015

@author: mkrcal
"""
import numpy as np
from simplex import *
from Cochain import *
#from cubical_sc import *
#from cubical_sc_torus import *

#outputs iterable of pairs of sets (a,b) such that we evaluate 
#<ext,\partial_a \Delta> * <ext,\partial_b \Delta> in the Steenrod formula
#n is n from the paper - we compute Sq(z^{n-1})
def compute_steenrod_pairs(n):
    result = set()
    for i1 in range(n+2):
        for i2 in range(i1+1,n+2):
            for i3 in range(i2+1,n+2):
                a,b = list(),list()
                if i1%2 == 0:
                    a.append(i1)
                else:
                    b.append(i1)
                if i2%2 == 1: #{1...i2} - {i1} is actually even
                    a.append(i2)
                else:
                    b.append(i2)
                if i3%2 == 0:
                    a.append(i3)
                else:
                    b.append(i3)
                if len(a)==1:
                    for i4 in [i for i in range(i3+1,n+2) if i%2 == 1]:
                        result.add((tuple(a+[i4]),tuple(b)))
                if len(b)==1:
                    for i4 in [i for i in range(i3+1,n+2) if i%2 == 0]:
                        result.add((tuple(a),tuple(b+[i4])))
    return result
                        
#print compute_steenrod_pairs(7)                        
                        

#n is n from the paper - we compute Sq(z^{n-1})
def steenrod_square(n,sc,cocycle,func_val_abs,func_sim,torus):
# cocycle is a dictionary 2-simplex:value

    def order_and_compute_sign(perm):
        perm_sign=1
        length=len(perm)
        for i in range(len(perm)):
            for j in range(i,length):
                if perm[i]>perm[j]:
                    perm_sign=-perm_sign
        return (sorted(perm),perm_sign)

    def order_vertices(sim):
        out=list()
        count=0
        for i in range(sim.dim+1):
            vert=sim.vertex(i)
            val=func_sim[tuple(vert)]
            # val can be -n,-n+1,...,-1,1,...,n
            if torus: out.append((count,val,tuple(sc.module(vert))))
            else: out.append((count,val,tuple(vert)))
            count+=1
        out=sorted(out,key=lambda item:(item[1],item[2])) # returns a sorted list of pairs (simplex, simp. value)
        return [x[0] for x in out]
        
    support=set()
    for sim in cocycle:
        cobound=sc.coboundary(sim)
        for cob in cobound:
            cobound2=sc.coboundary(cob[0])
            for cob2 in cobound2:
                support.add(cob2[0])
    
    steenrod_pairs = compute_steenrod_pairs(n)
 
#    support=sc.k_simplices_list(4) # UNOPTIMIZED :
    output=Cochain({}) # list of pairs  ((n+1)-simplex: value)
    for item in support: # item is a 4-simplex (as a tuple)
        seq_ext=[[0]*item.dim_ambient] + item.sequence_
        perm=order_vertices(item) # r0: initial r0
        # it returns a permutation of [0,1,2,3,4]
        tmp,perm_sign=order_and_compute_sign(perm)
        value = 0
        for (a,b) in steenrod_pairs:
            part1 = [perm[i] for i in range(len(perm)) if not i in a]
            part2 = [perm[i] for i in range(len(perm)) if not i in b]
            part1_ordered, perm_sim1=order_and_compute_sign(part1) # ([1,2,4],-1)
            part2_ordered, perm_sim2=order_and_compute_sign(part2) # ([0,1,3],+1)
            vert1=np.array(item.vertex_)+np.array(seq_ext[part1_ordered[0]])
            if torus: vert1=sc.module(vert1)
            seq1=[list(np.array(seq_ext[i])-np.array(seq_ext[part1_ordered[0]])) 
                for i in part1_ordered[1:]]
            simplex1=simplex(vert1,seq1)
            vert2=np.array(item.vertex_)+np.array(seq_ext[part2_ordered[0]])
            if torus: vert2=sc.module(vert2)
            seq2=[list(np.array(seq_ext[i])-np.array(seq_ext[part2_ordered[0]])) 
                for i in part2_ordered[1:]]
            simplex2=simplex(vert2,seq2)
            if simplex1 in cocycle and simplex2 in cocycle:
                value+=perm_sim1*perm_sim2*perm_sign*cocycle[simplex1]*cocycle[simplex2]
#        if value%2 == 1:
        if value != 0:
            output.add_singleton(item,value)
    return output
    
def cup_prod_dim2_cubical(sc,cocycle1, cocycle2):
    support=set()
    for item in set(cocycle1.keys()) | set(cocycle2.keys()):
        cob1=sc.cubical_coboundary(item)
        for item1 in cob1:
            cob2=sc.cubical_coboundary(item1[0])
            for item2 in cob2:
                support.add(item2[0])
    output=Cochain()
    for Sigma in support:
        print("analyzing cube", sc.cube_twice_barycenter(Sigma))
        result=0
        print("  partial result:", result)
        result +=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,1,1),0,1),0)* \
                cocycle2.get(sc.cube_face(sc.cube_face(Sigma,3,1),2,1),0)

        print("  partial result:", result)
        result -=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,2,1),0,1),0)* \
                    cocycle2.get(sc.cube_face(sc.cube_face(Sigma,3,1),1,1),0)

        print("  partial result:", result)
        result +=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,2,1),1,1),0)* \
                cocycle2.get(sc.cube_face(sc.cube_face(Sigma,3,1),0,1),0)
        
        print("  partial result:", result)
        result +=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,3,1),0,1),0)* \
                cocycle2.get(sc.cube_face(sc.cube_face(Sigma,2,1),1,1),0)
        
        print("  partial result:", result)
        result -=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,3,1),1,1),0)* \
                    cocycle2.get(sc.cube_face(sc.cube_face(Sigma,2,1),0,1),0)

        print("  partial result:", result)        
        result +=  cocycle1.get(sc.cube_face(sc.cube_face(Sigma,3,1),2,1),0)* \
                cocycle2.get(sc.cube_face(sc.cube_face(Sigma,1,1),0,1),0)
        print("  final result:", result)
        if result: 
            output[Sigma] = result
    return output

def steenrod_square_cubical(sc,cocycle):    
  def product(cocycle,sigma,a,partition):#a=[a0,a1,a2,a3] where a0<a1<a2<a3
      face1 = sigma
      face2 = sigma
      for i in range(3,-1,-1):
         if partition[i]==1:
             face1 = sc.cube_face(face1,a[i],(-1)**(a[i]-i)) #face1+=[a[i]]+=(partition[i]  )*(1**(a[i]-i))       
         else:
             face2 = sc.cube_face(face2,a[i],-(-1)**(a[i]-i))#face2+=[a[i]]+=(partition[i]-1)*(1**(a[i]-i))
      return cocycle.get(face1,0)*cocycle.get(face2,0);
  
  if cocycle=={}:
      return Cochain(cocycle)

  dim = sc.cube_dim(list(cocycle.keys())[0])
  #print("    input cocycle is nonempty of dimension ",dim)    
  square = Cochain()
  support = set()
  for item1 in cocycle.keys():
      for item2 in sc.cubical_coboundary(item1):
          for item3 in sc.cubical_coboundary(item2[0]):
              support.add(item3[0])
  for sigma in support:
     #print "sigma dimenze dim+2:",sigma
     result=0
     for a0 in range(0,dim-1):
         for a1 in range(a0+1,dim):
             for a2 in range(a1+1,dim+1):
                 for a3 in range(a2+1,dim+2):
                     a=[a0,a1,a2,a3]
                     result +=  product(cocycle,sigma,a,[1,1,0,0]) 
                     result +=  product(cocycle,sigma,a,[1,0,1,0]) 
                     result +=  product(cocycle,sigma,a,[1,0,0,1]) 
                     result +=  product(cocycle,sigma,a,[0,1,1,0]) 
                     result +=  product(cocycle,sigma,a,[0,1,0,1]) 
                     result +=  product(cocycle,sigma,a,[0,0,1,1])                
     #print result
     if result%2: 
         square[sigma] = result%2
  return square
  


def cup_square_dim2(sc,two_cochain1,two_cochain2,func_val_abs,func_sim,torus):
# two_cochain is a dictionary 2-simplex:value
    def order_and_compute_sign(perm):
        perm_sign=1
        length=len(perm)
        for i in range(len(perm)):
            for j in range(i,length):
                if perm[i]>perm[j]:
                    perm_sign=-perm_sign
        return (sorted(perm),perm_sign)

    def order_vertices(sim):
        out=list()
        count=0
        for i in range(sim.dim+1):
            vert=sim.vertex(i)
            val=func_sim[tuple(vert)]
            # val can be -n,-n+1,...,-1,1,...,n
            if torus: out.append((count,val,tuple(sc.module(vert))))
            else: out.append((count,val,tuple(vert)))
            count+=1
        out=sorted(out,key=lambda item:(item[1],item[2])) # returns a sorted list of pairs (simplex, simp. value)
        return [x[0] for x in out]

    four_sim=set()
    for sim in set(two_cochain1.keys()) | set(two_cochain2.keys()):
        cobound=sc.coboundary(sim)
        for cob in cobound:
            cobound2=sc.coboundary(cob[0])
            for cob2 in cobound2:
                four_sim.add(cob2[0])
 
    output=Cochain({}) # list of pairs  (4-simplex: value)
    for item in four_sim: # item is a 4-simplex (as a tuple)
        seq_ext=[[0]*item.dim_ambient] + item.sequence_
        perm=order_vertices(item) # r0: initial r0
        # it returns a permutation of [0,1,2,3,4]
        tmp,perm_sign=order_and_compute_sign(perm)
        part1=perm[0:3]
        part2=perm[2:5] # splits [4,2,1,3,0] to ([4,2,1],[1,3,0])
        part1_ordered, perm_sim1=order_and_compute_sign(part1) # ([1,2,4],-1)
        part2_ordered, perm_sim2=order_and_compute_sign(part2) # ([0,1,3],+1)
        vert1=np.array(item.vertex_)+np.array(seq_ext[part1_ordered[0]])
        if torus: vert1=sc.module(vert1)
        seq1=[list(np.array(seq_ext[i])-np.array(seq_ext[part1_ordered[0]])) for i in part1_ordered[1:]]
        simplex1=simplex(vert1,seq1)
        vert2=np.array(item.vertex_)+np.array(seq_ext[part2_ordered[0]])
        if torus: vert2=sc.module(vert2)
        seq2=[list(np.array(seq_ext[i])-np.array(seq_ext[part2_ordered[0]])) for i in part2_ordered[1:]]
        simplex2=simplex(vert2,seq2)
        if simplex1 in two_cochain1 and simplex2 in two_cochain2:
            value=perm_sim1*perm_sim2*perm_sign*two_cochain1[simplex1]*two_cochain2[simplex2]
            if value != 0:
                output.add_singleton(item,value)
    return output

#print compute_steenrod_pairs(3)
if __name__=="__main__":
#if False:
    from cubical_sc_torus import *
    sc=cubical_sc_torus([5,5,5,5])
    cube1=sc.twice_baryc2cube([1,1,2,2])
    cube2=sc.twice_baryc2cube([2,1,1,2])
    cube3=sc.twice_baryc2cube([2,2,1,1])
    c1=Cochain({cube1:1})
    print("cochain c1:")
    for item in c1:
        print("  cube:", sc.cube_twice_barycenter(item))
        print("  value:", c1[item])
    c2=Cochain({cube3:1})
    print("cochain c2:")
    for item in c2:
        print("  cube:", sc.cube_twice_barycenter(item))
        print("  value:", c2[item])
    prod = cup_prod_dim2_cubical(sc,c1,c2)
    print("cup prod(c1,c2)")
    for item in prod:
        print(" cube:", sc.cube_twice_barycenter(item))
        print(" value:", prod[item])
    
    '''    
    from cubical_sc import *
    from hom_algebra import *
    sc = cubical_sc([4,4,4,4,4,4])
    cocycle = Cochain({Cube([2,2,1,1,2,2]):1,Cube([1,1,0,0,0,0]):1})
    codiff_coc=codifferential_cubic(sc,cocycle)
    steen=steenrod_square_cubical(sc,codiff_coc)
    print(codifferential_cubic(sc,steen))
#    print cup_square_cubical(sc,cocycle)
    '''
